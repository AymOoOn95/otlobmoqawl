package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Post

class MyAddsAdapter(
    val posts: ArrayList<Post>,
    val context: Context?,
    val onBidSelectedClickListener: MyAddsBidsAdapter.OnBidSelectedClickListener
) :
    RecyclerView.Adapter<MyAddsAdapter.MyAddsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAddsViewHolder {
        return MyAddsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.my_adds_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = posts.size

    override fun onBindViewHolder(holder: MyAddsViewHolder, position: Int) {
        holder.loader.visibility = View.GONE
        posts[position].bids?.let {
            if (it.isNotEmpty()) {
                holder.bidsRecycler.visibility = View.VISIBLE
                holder.bidsRecycler.adapter =
                    MyAddsBidsAdapter(posts[position], it, context, onBidSelectedClickListener)
                holder.bidsRecycler.layoutManager = LinearLayoutManager(holder.bidsRecycler.context)
            } else {
                holder.noItem.visibility = View.VISIBLE
            }
        }
        holder.postTitle.text = posts[position].title
    }

    class MyAddsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val bidsRecycler: RecyclerView = itemView.findViewById(R.id.bids_recycler)
        val postTitle: TextView = itemView.findViewById(R.id.post_title)
        val noItem: TextView = itemView.findViewById(R.id.no_item)
        val loader: ProgressBar = itemView.findViewById(R.id.loader)
    }
}