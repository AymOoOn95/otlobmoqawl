package com.yahya.otlobmokawl.ui.fragment.account

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AccountViewModel(val userRepo: IUserRepo) : ViewModel() {

    fun changeLang(lang: String) {
        userRepo.changeLanguage(lang)
    }

    fun getLang(): String? {
        return userRepo.getLanguage()
    }

    fun logout() {
        userRepo.logout()
    }

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}