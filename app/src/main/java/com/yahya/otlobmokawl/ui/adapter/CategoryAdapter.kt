package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.transaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Category
import com.yahya.otlobmokawl.ui.fragment.posts.allposts.AllPostsFragment
import com.yahya.otlobmokawl.util.Constants

class CategoryAdapter(val categories: ArrayList<Category>, val context: Context?) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        return CategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.category_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.categoryName.text = categories[position].name
        Glide.with(context!!).load(categories[position].image).into(holder.categoryImage)
        holder.itemView.setOnClickListener {
            context?.let {
                val fragment = AllPostsFragment()
                fragment.arguments = bundleOf(Pair(Constants.CATEGORY_ID_ARGUMENT_KEY, categories[position].id))
                (it as AppCompatActivity).supportFragmentManager.transaction {
                    replace(R.id.fragment_container, fragment)
                }
            }

        }
    }

    override fun getItemCount(): Int = categories.size

    class CategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var categoryImage: ImageView
        var categoryName: TextView

        init {
            categoryImage = itemView.findViewById(R.id.category_image)
            categoryName = itemView.findViewById(R.id.category_name)
        }

    }
}