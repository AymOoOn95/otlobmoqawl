package com.yahya.otlobmokawl.ui.fragment.client.register

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

class ClientRegisterViewModel(val userRepo: IUserRepo, val clientRepo: IClientRepo) : ViewModel() {

    fun getLocations(parentId: Int?): Observable<ArrayList<Address>> {
        return (userRepo as UserRepo).getLocations(parentId)
    }

    fun register(client: Client, images: ArrayList<MultipartBody.Part?>): Observable<RegisterResponse> {
        return clientRepo.registerClient(client, images)
    }

    fun saveClientInShared(client: Client) {
        userRepo.saveUserData(client)
    }

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun userCreditOnline(apiToken: String): Observable<UserCredit> {
        return userRepo.myCredits(apiToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveCredits(userCredit: UserCredit) {
        userRepo.saveCredit(userCredit)
    }
}