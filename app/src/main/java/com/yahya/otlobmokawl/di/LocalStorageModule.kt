package com.yahya.otlobmokawl.di

import android.content.Context
import android.content.SharedPreferences
import com.yahya.otlobmokawl.util.Constants
import dagger.Module
import dagger.Provides

@Module
class LocalStorageModule(val context: Context) {

    @Provides
    fun provideUserSharedFile(): SharedPreferences {
        return context.getSharedPreferences(Constants.LOGIN_SHARED_FILE_NAME, Context.MODE_PRIVATE)
    }
}