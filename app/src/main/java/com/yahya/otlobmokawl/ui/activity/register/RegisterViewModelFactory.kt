package com.yahya.otlobmokawl.ui.activity.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class RegisterViewModelFactory(val userRepo: IUserRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return RegisterViewModel(userRepo) as T
    }
}