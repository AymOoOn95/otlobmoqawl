package com.yahya.otlobmokawl.ui.fragment.contractor.details

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.databinding.AddsBidsDetailsBinding
import com.yahya.otlobmokawl.util.Constants

class ContractorDetailsFragment : Fragment() {
    var binding: AddsBidsDetailsBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.adds_bids_details, container, false)
        arguments?.getParcelable<Post>(Constants.POST_ARGU_KEY)?.let {
            binding?.post = it
        }
        return binding?.root
    }
}

@BindingAdapter("url")
fun loadImage(imageView: ImageView, url: String) {
    Glide.with(imageView).load(url).into(imageView)
}