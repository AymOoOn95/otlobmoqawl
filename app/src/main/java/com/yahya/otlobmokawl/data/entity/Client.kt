package com.yahya.otlobmokawl.data.entity

import android.os.Parcelable
import com.yahya.otlobmokawl.util.Constants
import kotlinx.android.parcel.Parcelize

@Parcelize
class Client(
    var userName: String?,
    var userEmail: String?,
    var userPassword: String?,
    var userPhone: String?,
    var userApiToken: String?,
    var userRating: Int?,
    var userImage: String?,
    var userType: String? = Constants.USER_TYPE_CLIENT,
    var userAreaId: Int?,
    var userAddress: String?,
    var projects: List<Post>?
) : User(
    userName,
    userEmail,
    userPassword,
    userPhone,
    userApiToken,
    userRating,
    userImage,
    userType,
    userAreaId,
    userAddress,
    userApiToken
), Parcelable {
    constructor() : this(null, null, null, null, null, null, null, null, null, null, null)
}