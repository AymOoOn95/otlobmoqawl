package com.yahya.otlobmokawl.ui.fragment.question

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.QuestionsFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.QuestionsAdapter
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class QuetionsFragment : Fragment() {
    var binding: QuestionsFragmentBinding? = null
    @Inject
    lateinit var userRepo: IUserRepo
    @Inject
    lateinit var clientRepo: IClientRepo

    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.questions_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: QuestionsViewModel = ViewModelProviders.of(this, QuestionsViewModelFactory(userRepo, clientRepo))
            .get(QuestionsViewModel::class.java)
        binding?.viewModel = viewModel
        compositeDisposable.add(viewModel.userData().subscribe { currentUser ->
            currentUser.apiToken?.let { apiToken ->
                compositeDisposable.add(viewModel.questions4Me(apiToken).subscribe {
                    binding?.loader?.visibility = View.GONE
                    if (it.isEmpty()) {
                        binding?.noItem?.visibility = View.VISIBLE
                    } else {
                        binding?.questionsRecycler?.visibility = View.VISIBLE
                        binding?.questionsRecycler?.adapter =
                            QuestionsAdapter(
                                it,
                                context,
                                currentUser.type!!,
                                object : QuestionsAdapter.OnSendAnswerClickListener {

                                    override fun onClick(question: Question) {
                                        val progressDialog = ProgressDialog.show(context, "Wait", "")
                                        compositeDisposable.add(viewModel.answerQuestion(question, apiToken).subscribe {
                                            progressDialog.dismiss()
                                            context?.makeToast(getString(R.string.done_answer))
                                            (binding?.questionsRecycler?.adapter as QuestionsAdapter).updateItem(it)

                                        })
                                    }
                                })
                        binding?.questionsRecycler?.layoutManager = LinearLayoutManager(context)
                    }
                })
            }
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}
