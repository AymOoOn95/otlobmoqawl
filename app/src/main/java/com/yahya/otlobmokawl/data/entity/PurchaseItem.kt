package com.yahya.otlobmokawl.data.entity

import com.google.gson.annotations.SerializedName

data class PurchaseItem(
    var id: Int,
    @SerializedName("view_posts")
    var viewPostsCredit: Int,
    @SerializedName("create_posts")
    var createPostsCredit: Int,
    @SerializedName("create_bids")
    var createBidCredit: Int,

    @SerializedName("payment_method")
    var paymentMethod: String,
    @SerializedName("created_at")
    var purchaseDate: String,

    var name: String,
    @SerializedName("amount")
    var price: String?

)