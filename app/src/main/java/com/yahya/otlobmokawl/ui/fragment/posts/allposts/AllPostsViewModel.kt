package com.yahya.otlobmokawl.ui.fragment.posts.allposts

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AllPostsViewModel(val projectRepo: IProjectRepo, val userRepo: IUserRepo) : ViewModel() {

    fun getUserData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getPosts(apiToken: String, categoryId: Int): Observable<ArrayList<Post>> {
        return projectRepo.getProjects(apiToken, categoryId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun addPost2Fav(postId: Int, apiToken: String): Observable<RegisterResponse> {
        return projectRepo.addToFavourite(postId, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}