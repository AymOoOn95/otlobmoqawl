package com.yahya.otlobmokawl.data.entity

import com.google.gson.annotations.SerializedName

class UserCredit(
    @SerializedName("api_token")
    var apiToken: String?,
    var status: Int,
    var posts: Int,
    var bids: Int
) {
}