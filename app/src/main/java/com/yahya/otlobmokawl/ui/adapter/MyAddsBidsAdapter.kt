package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Post

class MyAddsBidsAdapter(
    val post: Post,
    val bids: List<Bid>,
    val context: Context?,
    val onBidSelectedClickListener: OnBidSelectedClickListener
) :
    RecyclerView.Adapter<MyAddsBidsAdapter.MyAddsBidsViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAddsBidsViewHolder {
        return MyAddsBidsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.bid_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = bids.size

    override fun onBindViewHolder(holder: MyAddsBidsViewHolder, position: Int) {
        holder.biddingNumber.text = "${position + 1}"
        holder.bidding.text = bids[position].bid
        holder.contractorRating.numStars = bids[position].contractor?.rating ?: 0
        holder.agreeBtn.setOnClickListener {
            onBidSelectedClickListener.onBidSelected(post, bids[position])
        }
    }

    interface OnBidSelectedClickListener {
        fun onBidSelected(post: Post, bid: Bid)
    }

    class MyAddsBidsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val agreeBtn: TextView = itemView.findViewById(R.id.agree_btn)
        val biddingNumber: TextView = itemView.findViewById(R.id.bid_number)
        val bidding: TextView = itemView.findViewById(R.id.bidding)
        val contractorRating: RatingBar = itemView.findViewById(R.id.contractor_rating)
    }
}