package com.yahya.otlobmokawl.data.entity

import com.yahya.otlobmokawl.util.Constants
import com.google.gson.annotations.SerializedName

class Contractor(
    name: String?,
    email: String?,
    password: String?,
    phone: String?,
    apiToken: String?,
    rating: Int?,
    image: String?,
    type: String? = Constants.USER_TYPE_CONTRACTOR,
    areaId: Int?,
    address: String?,
    sid: String?,
    var about: String?,
    @SerializedName("job")
    var jobTitle: String?,
    var previousWork: List<Post>?
) : User(
    name,
    email,
    password,
    phone,
    apiToken,
    rating,
    image,
    type,
    areaId,
    address,
    sid

) {
    constructor() : this(null, null, null, null, null, null, null, null, null, null, null, null, null, null)

    override fun toString(): String {
        return this.name ?: ""
    }
}

