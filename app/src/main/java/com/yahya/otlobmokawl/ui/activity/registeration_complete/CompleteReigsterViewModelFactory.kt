package com.yahya.otlobmokawl.ui.activity.registeration_complete

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class CompleteReigsterViewModelFactory(val userRepo: IUserRepo) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return CompleteRegisterViewModel(userRepo) as T
    }
}