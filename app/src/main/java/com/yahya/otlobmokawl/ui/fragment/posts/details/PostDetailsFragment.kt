package com.yahya.otlobmokawl.ui.fragment.posts.details

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.ui.adapter.ImagesSliderAdapter
import com.yahya.otlobmokawl.ui.adapter.QuestionAndAnswerAdapter
import com.yahya.otlobmokawl.ui.fragment.bid.view.PostBidFragment
import com.yahya.otlobmokawl.ui.fragment.posts.allposts.AllPostsFragment
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject
import android.net.Uri
import com.facebook.share.model.ShareLinkContent
import com.facebook.share.widget.ShareDialog
import com.yahya.otlobmokawl.databinding.PostDetailsFragmentBinding


class PostDetailsFragment : Fragment() {

    var binding: PostDetailsFragmentBinding? = null

    @Inject
    lateinit var userRepo: IUserRepo
    @Inject
    lateinit var contractorRepo: IContractorRepo

    @Inject
    lateinit var projectRepo: IProjectRepo

    val compositeDisposable = CompositeDisposable()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.post_details_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: PostDetailsViewModel =
            ViewModelProviders.of(this, PostDetailsViewModelFactory(projectRepo, contractorRepo, userRepo))
                .get(PostDetailsViewModel::class.java)

        activity?.findViewById<Button>(R.id.add_new)?.visibility = View.GONE
        arguments?.let {
            val post: Post? = it.getParcelable(Constants.POST_ARGU_KEY)
            post?.let { nonNullablePost ->
                Log.d("TAAAAAAAAAAG", "${nonNullablePost.images?.size}")
                Log.d("PostDetailsFragment", "post_id ==> ${nonNullablePost.id}")
                compositeDisposable.add(viewModel.userData().subscribe { currentUser ->
                    binding?.let { nonNullableBinding ->
                        if (nonNullablePost.isAdd == 1) {
                            nonNullableBinding.makeOffer.visibility = View.GONE
                            nonNullableBinding.askQuestionBtn.visibility = View.GONE
                            nonNullableBinding.seeOffers.visibility = View.GONE
                            nonNullableBinding.postQuestion.visibility = View.GONE
                            nonNullableBinding.seeOffersCard.visibility = View.GONE
                        }
                        nonNullableBinding.userType = currentUser.type
                        nonNullableBinding.isMine = nonNullablePost.isMine == 1
                        if (nonNullablePost.isInFavourite == 1) {
                            nonNullableBinding.add2FavBtn.setImageResource(R.drawable.ic_fav_filled)
                        }
                        nonNullableBinding.add2FavBtn.setOnClickListener {
                            viewModel.addPost2Fav(nonNullablePost.id!!, currentUser.apiToken!!).subscribe {
                                if (it.status == 1) {
                                    if (it.isDeleted == 1) {
                                        context?.makeToast(getString(R.string.deleted_from_fav))
                                        nonNullableBinding.add2FavBtn.setImageResource(R.drawable.ic_empty_fav_primary)
                                    } else {
                                        context?.makeToast(getString(R.string.added_2_fav))
                                        nonNullableBinding.add2FavBtn.setImageResource(R.drawable.ic_fav_filled)

                                    }
                                }
                            }
                        }
                        nonNullableBinding.backBtn.setOnClickListener {
                            val fragment = AllPostsFragment()
                            fragment.arguments =
                                bundleOf(Pair(Constants.CATEGORY_ID_ARGUMENT_KEY, nonNullablePost.categoryId))
                            (context as AppCompatActivity).supportFragmentManager.transaction {
                                replace(R.id.fragment_container, fragment)
                            }
                        }

                        nonNullablePost.images?.let {
                            (nonNullableBinding.postImages as RecyclerView).adapter = ImagesSliderAdapter(it, context)
                            (nonNullableBinding.postImages as RecyclerView).layoutManager = LinearLayoutManager(
                                context,
                                LinearLayoutManager.HORIZONTAL,
                                false
                            )

                            nonNullableBinding.recyclerviewPagerIndicatorHorizontal?.attachToRecyclerView(
                                nonNullableBinding.postImages
                            )
                        }


                        currentUser.apiToken?.let {
                            compositeDisposable.add(viewModel.postQuestion(nonNullablePost.id!!, it).subscribe {
                                nonNullableBinding.loader?.visibility = View.GONE
                                if (it.isNotEmpty()) {
                                    nonNullableBinding.questionsRecycler.visibility = View.VISIBLE
                                    nonNullableBinding.questionsRecycler.adapter = QuestionAndAnswerAdapter(it)
                                    nonNullableBinding.questionsRecycler.layoutManager =
                                        LinearLayoutManager(context)
                                } else {
                                    nonNullableBinding.noItem?.visibility = View.VISIBLE
                                }
                            })
                        }

                        nonNullableBinding.shareBtn.setOnClickListener {
                            val shareObj = ShareLinkContent.Builder()
                                .setContentUrl(Uri.parse(nonNullablePost.image))
                                .setQuote("Otlob Mokawl")
                                .build()

                            ShareDialog.show(this, shareObj)

                        }

                        nonNullableBinding.postTitle.text = nonNullablePost.title
                        nonNullableBinding.categoryTitle.text = nonNullablePost.subCategoryStr
                        nonNullableBinding.postContent.text = nonNullablePost.content
//                        nonNullableBinding.questionsRecycler.adapter = QuestionAndAnswerAdapter(ArrayList<Question>())
//                        nonNullableBinding.questionsRecycler.layoutManager = LinearLayoutManager(context)

                        nonNullableBinding.seeOffers.setOnClickListener {
                            val postBidFragment = PostBidFragment()
                            postBidFragment.arguments = bundleOf(
                                Pair(Constants.POST_ARGU_KEY, nonNullablePost),
                                Pair(Constants.API_TOKEN_ARGU_KEY, currentUser.apiToken)
                            )
                            activity?.supportFragmentManager?.transaction {
                                replace(R.id.fragment_container, postBidFragment)
                            }
                        }
                        nonNullableBinding.makeOffer.setOnClickListener {
                            compositeDisposable.add(viewModel.myCredits().subscribe {
                                when (it.status) {
                                    Constants.STATUS_SUCCESS -> {
                                        if (it.bids > 0) {
                                            var dialog: AlertDialog =
                                                AlertDialog.Builder(context!!).setView(R.layout.make_bid_dialog)
                                                    .show()
                                            dialog.window.setBackgroundDrawableResource(R.color.complete_transparent)
                                            dialog.findViewById<Button>(R.id.send_bid)?.setOnClickListener {
                                                currentUser.apiToken?.let { apiToken ->
                                                    dialog.dismiss()
                                                    val bidStr: String =
                                                        dialog.findViewById<EditText>(R.id.bid_str)?.text?.toString()
                                                            ?: ""
                                                    if (bidStr.isNotEmpty()) {
                                                        val bid =
                                                            Bid(
                                                                null,
                                                                nonNullablePost.id,
                                                                null,
                                                                bidStr,
                                                                null,
                                                                null,
                                                                null
                                                            )
                                                        compositeDisposable.add(
                                                            viewModel.sendBid(
                                                                apiToken,
                                                                bid
                                                            ).subscribe {
                                                                var msg = getString(R.string.bid_already_sent)

                                                                if (it.status == 1) {
                                                                    msg = getString(R.string.bid_sent)
                                                                    var userCreditStatus = 0
                                                                    if (it.availableBids > 0) {
                                                                        userCreditStatus = 1
                                                                    }
                                                                    viewModel.saveUserCredit(
                                                                        UserCredit(
                                                                            null,
                                                                            userCreditStatus,
                                                                            0,
                                                                            it.availableBids
                                                                        )
                                                                    )

                                                                }
                                                                context?.makeToast(msg)


                                                            })
                                                    }

                                                }
                                            }
                                        } else {
                                            context?.makeToast(getString(R.string.not_enough_credit))

                                        }
                                    }
                                    Constants.STATUS_BANNED -> {
                                        context?.makeToast(getString(R.string.user_banned))
                                    }
                                }
                            })
                        }
                        Glide.with(context!!).load(arguments?.getString(Constants.CATEGORY_IMAGE_ARGU_KEY))
                            .into(nonNullableBinding.categoryImage)

                        nonNullableBinding.askQuestionBtn.setOnClickListener {
                            var dialog: AlertDialog =
                                AlertDialog.Builder(context!!).setView(R.layout.ask_question_dialog)
                                    .show()
                            dialog.window.setBackgroundDrawableResource(R.color.complete_transparent)
                            dialog.findViewById<Button>(R.id.cancel)?.setOnClickListener {
                                dialog.dismiss()
                            }

                            dialog.findViewById<Button>(R.id.ask)?.setOnClickListener {
                                dialog.dismiss()

                                currentUser.apiToken?.let {
                                    val question: String =
                                        dialog.findViewById<EditText>(R.id.question)!!.text.toString()
                                    if (question.isNotEmpty()) {
                                        val phoneFounded = "\\d{10,14}".toRegex().find(question)?.value
                                        if (phoneFounded == null) {
                                            viewModel.askQuestion(
                                                Question(
                                                    null,
                                                    null,
                                                    nonNullablePost.title,
                                                    question,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null,
                                                    null
                                                ),
                                                it,
                                                nonNullablePost.id!!
                                            ).subscribe {
                                                context?.makeToast(getString(R.string.question_added))
//                                            dialog.dismiss()
                                            }
                                        } else {
                                            context?.makeToast(resources.getString(R.string.typing_phone))
                                        }
                                    } else {
                                        context?.makeToast(getString(R.string.write_question))
//                                        dialog.dismiss()
                                    }
                                }

                            }
                        }
                    }
                })
            }
        }
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
        activity?.findViewById<Button>(R.id.add_new)?.visibility = View.VISIBLE
    }
}