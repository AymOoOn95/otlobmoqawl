package com.yahya.otlobmokawl.ui.fragment.memberships

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Membership
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MembershipsViewModel(val repo: IUserRepo) : ViewModel() {
    fun getMemberships(): Observable<ArrayList<Membership>> {
        return repo.memberships().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}