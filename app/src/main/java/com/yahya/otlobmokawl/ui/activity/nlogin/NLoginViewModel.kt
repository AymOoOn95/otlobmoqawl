package com.yahya.otlobmokawl.ui.activity.nlogin

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NLoginViewModel(val userRepo: IUserRepo) : ViewModel() {

    fun login(user: User): Observable<RegisterResponse> {
        return userRepo.login(user).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun checkLogin(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveUserData(user: User) {
        userRepo.saveUserData(user)
    }

    fun anonymousRegister(user: User, idToken: String): Observable<RegisterResponse> {
        return userRepo.register(user, idToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getLang(): String {
        return userRepo.getLanguage()
    }

    fun userCreditOnline(apiToken: String): Observable<UserCredit> {
        return userRepo.myCredits(apiToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveCredits(userCredit: UserCredit) {
        userRepo.saveCredit(userCredit)
    }
}