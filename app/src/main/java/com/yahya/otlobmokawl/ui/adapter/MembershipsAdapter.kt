package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Membership

class MembershipsAdapter(val memberships: ArrayList<Membership>, val context: Context?) :
    RecyclerView.Adapter<MembershipsAdapter.MembershipViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MembershipViewHolder = MembershipViewHolder(
        LayoutInflater.from(parent.context).inflate(
            R.layout.in_app_purchase_item_layout,
            parent,
            false
        )
    )

    override fun getItemCount(): Int = memberships.size

    override fun onBindViewHolder(holder: MembershipViewHolder, position: Int) {
        holder.buyItem.visibility = View.GONE
        holder.itemTitle.text = memberships[position].name
        holder.itemDesc.text = memberships[position].desc
        holder.itemPrice.text = context?.resources?.getString(R.string.item_price, memberships[position].cost) ?: "0"
    }

    class MembershipViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemTitle: TextView = itemView.findViewById(R.id.item_title)
        val itemDesc: TextView = itemView.findViewById(R.id.item_desc)
        val itemPrice: TextView = itemView.findViewById(R.id.item_price)
        val buyItem: ImageView = itemView.findViewById(R.id.buy_item)
    }
}