package com.yahya.otlobmokawl.data.repocontract

import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Contractor
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.network.response.AddBidResponse
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import io.reactivex.Observable
import okhttp3.MultipartBody

interface IContractorRepo {
    fun makeBid(bid: Bid, apiToken: String): Observable<AddBidResponse>
    fun removeBid(bid: Bid)
    fun askQuestion(question: Question, apiToken: String, postId: Int): Observable<RegisterResponse>
    fun uploadImage(project: Post, images: List<String>)
    fun updateContractor(
        contractor: Contractor,
        newPassword: String?,
        imagePart: MultipartBody.Part?
    ): Observable<RegisterResponse>

    fun getContractorProjects(contractor: Contractor): Observable<List<Post>>
    fun contractorRegister(contractor: Contractor, images: ArrayList<MultipartBody.Part?>): Observable<RegisterResponse>

}