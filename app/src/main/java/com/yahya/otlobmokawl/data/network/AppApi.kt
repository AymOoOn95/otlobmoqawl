package com.yahya.otlobmokawl.data.network

import com.yahya.otlobmokawl.data.entity.*
import com.yahya.otlobmokawl.data.network.response.*
import com.yahya.otlobmokawl.util.Constants
import io.reactivex.Observable
import okhttp3.MultipartBody
import retrofit2.http.*

interface AppApi {

    @GET("areas")
    fun getLocations(@Query("country_id") countryId: Int?, @Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<Address>>


    @Multipart
    @POST("post/register")
    fun register(
        @Query("id_token") idToken: String?,
        @Query("email") email: String?,
        @Query("phone_number") phone: String?,
        @Query("name") name: String?,
        @Query("password") password: String?,
        @Query("user_type") userType: String?,
        @Query("nation_id") sid: String?,
        @Query("area_id") areaId: Int?,
        @Query("address") address: String?,
        @Query("about") about: String?,
        @Query("job") jobTitle: String?,
        @Part images: ArrayList<MultipartBody.Part?>?,
        @Part dummy: MultipartBody.Part = MultipartBody.Part.createFormData("dummy", "dummy")
    ): Observable<RegisterResponse>


    @Multipart
    @POST("post/complete/register")
    fun completeRegister(
        @Query("api_token") apiToken: String?,
        @Query("email") email: String?,
        @Query("phone_number") phone: String?,
        @Query("name") name: String?,
        @Query("password") password: String?,
        @Query("user_type") userType: String?,
        @Query("nation_id") sid: String?,
        @Query("area_id") areaId: Int?,
        @Query("address") address: String?,
        @Query("about") about: String?,
        @Query("job") jobTitle: String?,
        @Part images: ArrayList<MultipartBody.Part?>?,
        @Part dummy: MultipartBody.Part = MultipartBody.Part.createFormData("dummy", "dummy")
    ): Observable<RegisterResponse>

    @POST("post/login")
    fun login(
        @Query("email") email: String?,
        @Query("phone_number") phone: String?,
        @Query("password") password: String?
    ): Observable<RegisterResponse>

    @GET("categories")
    fun categories(@Query("parent_id") parentId: Int? = null, @Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<Category>>

    @GET("posts/category/{category_id}")
    fun categoryPosts(@Path("category_id") categoryId: Int, @Query("api_token") apiToken: String, @Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<Post>>

    @Multipart
    @POST("create/post")
    fun addPost(
        @Query("api_token") apiToken: String,
        @Query("category_id") categoryId: Int,
        @Query("sub_category") subCategoryId: Int,
        @Query("area_id") areaId: Int,
        @Query("title") title: String,
        @Query("content") content: String,
        @Query("address") address: String,
        @Query("is_contractor") isContractor: String?,
        @Part images: ArrayList<MultipartBody.Part?>?,
        @Part dummy: MultipartBody.Part = MultipartBody.Part.createFormData("dummy", "dummy")
    ): Observable<AddPostResponse>

    @POST("set-favorite/post/{post_id}")
    fun addPost2Fav(@Path("post_id") postId: Int, @Query("api_token") apiToken: String, @Query("_method") method: String = "PATCH"): Observable<RegisterResponse>

    @POST("ask/question/{post_id}")
    fun askQuestion(@Path("post_id") postId: Int, @Query("api_token") apiToken: String, @Query("question") question: String): Observable<RegisterResponse>

    @GET("my-list")
    fun myList(@Query("api_token") apiToken: String, @Query("lang") lang: String? = Constants.LANG_AR): Observable<MyListResponse>

    @GET("my-favorite")
    fun favourites(@Query("api_token") apiToken: String, @Query("lang") lang: String? = Constants.LANG_AR): Observable<MyListResponse>

    @GET("view/questions")
    fun questions4Me(@Query("api_token") apiToken: String, @Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<Question>>


    @GET("view/questions/{post_id}")
    fun postQuestions(@Path("post_id") postId: Int, @Query("api_token") apiToken: String, @Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<Question>>

    @POST("answer/question/{question_id}")
    fun answerQuestion(
        @Path("question_id") postId: Int?, @Query("api_token") apiToken: String, @Query("answer") answer: String?,
        @Query("_method") method: String? = "PATCH"
    ): Observable<Question>

    @POST("set-bid/post/{post_id}")
    fun makeOffer(@Path("post_id") postId: Int, @Query("api_token") apiToken: String, @Query("bid") bid: String): Observable<AddBidResponse>

    @GET("view/bids/{post_id}")
    fun postBids(@Path("post_id") postId: Int, @Query("api_token") apiToken: String): Observable<ArrayList<Bid>>

    @POST("approve-bid/post/{bid_Id}")
    fun selectBid(@Path("bid_Id") bidId: Int, @Query("api_token") apiToken: String, @Query("_method") method: String = "PATCH"): Observable<Bid>

    @GET("user")
    fun userData(@Query("api_token") apiToken: String): Observable<UserDataResponse>

    @GET("payment-methods")
    fun paymentMethods(@Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<PaymentMethods>>

    @GET("my-credit")
    fun myCredits(@Query("api_token") apiToken: String): Observable<UserCredit>

    @GET("my-invoices")
    fun purchaseItems(@Query("api_token") apiToken: String, @Query("lang") lang: String? = Constants.LANG_AR): Observable<PurchaseReportResponse>

    @POST("create-invoice")
    fun makePurchase(
        @Query("api_token") apiToken: String,
        @Query("payment_methode") paymentMethod: Int = 5,
        @Query("sku") sku: String,
        @Query("orderId") orderId: String,
        @Query("purchase") purchaseToken: String,
        @Query("signature") signature: String,
        @Query("purchaseTime") purchaseTime: String,
        @Query("price") price: String,
        @Query("currencyCode") currency: String
    ): Observable<BuyCreditResponse>

    @GET("privacy")
    fun privacyPolicy(@Query("lang") lang: String? = Constants.LANG_AR): Observable<InformativeResponse>


    @GET("about")
    fun about(@Query("lang") lang: String? = Constants.LANG_AR): Observable<InformativeResponse>


    @GET("memberships")
    fun memberships(@Query("lang") lang: String? = Constants.LANG_AR): Observable<ArrayList<Membership>>

    @Multipart
    @POST("update/info")
    fun updateAccount(
        @Query("api_token") apiToken: String?,
        @Query("oldPassword") oldPassword: String?,
        @Query("newPassword") newPassword: String?,
        @Query("name") name: String?,
        @Query("area_id") areaId: Int?,
        @Query("address") address: String?,
        @Query("phone_number") phoneNumber: String?,
        @Query("job") job: String?,
        @Query("about") about: String?,
        @Part imagePart: MultipartBody.Part?,
        @Part dummy: MultipartBody.Part = MultipartBody.Part.createFormData("dummy", "dummy"),
        @Query("_method") method: String? = "PATCH"

    ): Observable<RegisterResponse>
}
