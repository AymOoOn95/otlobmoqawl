package com.yahya.otlobmokawl.ui.activity.splash

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SplashViewModel(private val userRepo: IUserRepo) : ViewModel() {

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun myCredits(apiToken: String): Observable<UserCredit> {
        return userRepo.myCredits(apiToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveCredits(userCredit: UserCredit) {
        userRepo.saveCredit(userCredit)
    }
}