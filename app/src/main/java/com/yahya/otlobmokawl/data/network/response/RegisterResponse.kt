package com.yahya.otlobmokawl.data.network.response

import com.yahya.otlobmokawl.data.entity.User
import com.google.gson.annotations.SerializedName

class RegisterResponse(
    val type: String?,
    val status: Int?,
    val user: User?,
    val role: Int?,
    @SerializedName("is_deleted")
    val isDeleted: Int?,
    @SerializedName("api_token")
    val apiToken: String?
) {

}