package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.net.toUri
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.util.toFile
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.yahya.otlobmokawl.util.toImagePart
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import java.io.File

class ImagesAdapter(val context: Context) :
    RecyclerView.Adapter<ImagesAdapter.ImageViewHolder>() {

    var images: ArrayList<File> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.image_item_layout, parent, false))
    }

    override fun getItemCount(): Int = 8

    fun getImagesAsFiles(): ArrayList<MultipartBody.Part?>? {
        val imagesPart: ArrayList<MultipartBody.Part?> = ArrayList()
        for (fileObj in images) {
            imagesPart.add(fileObj.toImagePart("images[]"))
        }
        Log.d("ImagesAdapter", "images count ${imagesPart.size}")
        return imagesPart
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {

        if (images.size > 0) {
            if (position < images.size) {
                holder.uploadImage.visibility = View.GONE
                holder.imageNumber.visibility = View.GONE
                holder.image.setImageURI(images[position].toUri())
                holder.image.visibility = View.VISIBLE
            }
            if (position == images.size) {
                holder.imageNumber.visibility = View.GONE
                holder.image.visibility = View.GONE
                holder.uploadImage.visibility = View.VISIBLE
                holder.uploadImage.setOnClickListener {
                    RxPaparazzo.multiple(context as AppCompatActivity).usingFiles().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe {
                            if (it.data() != null) {
                                val availabeSize = itemCount - images.size
                                Log.d("ImagesAdapter", " available size $availabeSize")
                                Log.d("ImagesAdapter", " item size ${it.data().size}")
                                if (it.data().size in 1..availabeSize) {
                                    for (fileData in it.data()) {
                                        images.add(fileData.file)
                                    }
                                    notifyDataSetChanged()
                                }
                            }
                        }
                }
            }

        } else {
            if (position == 0) {
                holder.imageNumber.visibility = View.GONE
                holder.uploadImage.visibility = View.VISIBLE
                holder.uploadImage.setOnClickListener {
                    RxPaparazzo.multiple(context as AppCompatActivity).usingFiles().subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread()).subscribe {
                            if (it.data() != null) {
                                if (it.data().size > 0) {
                                    for (fileData in it.data()) {
                                        images.add(fileData.file)
                                    }
                                    notifyDataSetChanged()
                                }
                            }
                        }
                }
            }
        }

        holder.imageNumber.findViewById<TextView>(R.id.image_number).text = "${position + 1}"
        holder.image.setOnClickListener {
            if (position < images.size) {
                RxPaparazzo.single(context as AppCompatActivity).usingFiles().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        if (it.data() != null) {
                            images.remove(images[position])
                            images.add(position, it.data().file)
                            notifyDataSetChanged()

                        }
                    }
            }
        }
    }

    class ImageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val uploadImage: LinearLayout = itemView.findViewById(R.id.upload_images)
        val imageNumber: LinearLayout = itemView.findViewById(R.id.image_number_layout)
        val image: ImageView = itemView.findViewById(R.id.image)

    }
}