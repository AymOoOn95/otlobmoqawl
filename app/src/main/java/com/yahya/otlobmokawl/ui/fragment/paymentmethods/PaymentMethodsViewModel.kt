package com.yahya.otlobmokawl.ui.fragment.paymentmethods

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.PaymentMethods
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PaymentMethodsViewModel(val userRepo: IUserRepo) : ViewModel() {

    fun paymentMetods(): Observable<ArrayList<PaymentMethods>> =
        (userRepo as UserRepo).paymentMethods().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

}