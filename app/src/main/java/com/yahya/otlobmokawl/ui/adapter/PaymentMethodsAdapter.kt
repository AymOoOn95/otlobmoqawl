package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.PaymentMethods

class PaymentMethodsAdapter(val paymentMethods: ArrayList<PaymentMethods>, val context: Context) :
    BaseExpandableListAdapter() {

    override fun getGroup(groupPosition: Int): Any = paymentMethods[groupPosition]

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean = true
    override fun hasStableIds(): Boolean = false

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        // todo to be refactored
        var myConverter = convertView
        if (myConverter == null) {
            myConverter = LayoutInflater.from(context).inflate(R.layout.payment_method_parent_view, parent, false)
        }

        myConverter!!.findViewById<TextView>(R.id.payment_method_name).text = paymentMethods[groupPosition].toString()
        myConverter.findViewById<ImageView>(R.id.indicator).isSelected = isExpanded
        var paymentMethodIcon: ImageView = myConverter.findViewById(R.id.payment_method_icon)
        Glide.with(paymentMethodIcon).load(paymentMethods[groupPosition].image).into(paymentMethodIcon)
        return myConverter
    }

    override fun getChildrenCount(groupPosition: Int): Int = paymentMethods[groupPosition].details?.size ?: 0

    override fun getChild(groupPosition: Int, childPosition: Int): Any =
        paymentMethods[groupPosition].details?.get(childPosition)!!

    override fun getGroupId(groupPosition: Int): Long = groupPosition.toLong()

    override fun getChildView(
        groupPosition: Int,
        childPosition: Int,
        isLastChild: Boolean,
        convertView: View?,
        parent: ViewGroup?
    ): View {
        // todo to be refactored
        var myConverter = convertView
        if (myConverter == null) {
            myConverter = LayoutInflater.from(context).inflate(R.layout.payment_method_child_view, parent, false)
        }
        myConverter!!.findViewById<TextView>(R.id.payment_method_details).text =
            paymentMethods[groupPosition].details?.get(childPosition)?.toString()

        return myConverter
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long = childPosition.toLong()

    override fun getGroupCount(): Int = paymentMethods.size

}