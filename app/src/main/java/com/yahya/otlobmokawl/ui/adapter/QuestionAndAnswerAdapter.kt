package com.yahya.otlobmokawl.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Question

class QuestionAndAnswerAdapter(val questions: ArrayList<Question>) :
    RecyclerView.Adapter<QuestionAndAnswerAdapter.QuestionAndAnswerViewHolder>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): QuestionAndAnswerViewHolder {
        return QuestionAndAnswerViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.question_and_answer_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = questions.size

    override fun onBindViewHolder(holder: QuestionAndAnswerViewHolder, position: Int) {
        holder.questionView.text = questions[position].question
        holder.answerView.text = questions[position].answer
    }

    class QuestionAndAnswerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val questionView: TextView = itemView.findViewById(R.id.questions)
        val answerView: TextView = itemView.findViewById(R.id.answer)

    }
}