package com.yahya.otlobmokawl.ui.activity.nlogin

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class NLoginViewModelFactory(val userRepo:IUserRepo):ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return NLoginViewModel(userRepo) as T
    }
}