package com.yahya.otlobmokawl.ui.fragment.posts.myposts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class MyPostsViewModelFactory(val userRepo: IUserRepo, val projectRepo: IProjectRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyPostsViewModel(userRepo, projectRepo) as T
    }
}