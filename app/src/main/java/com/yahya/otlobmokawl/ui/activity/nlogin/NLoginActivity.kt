package com.yahya.otlobmokawl.ui.activity.nlogin

import android.app.ProgressDialog
import android.content.Intent
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.ActivityNloginBinding
import com.yahya.otlobmokawl.ui.activity.initialregister.InitialRegisterActivity
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.ui.activity.register.RegisterActivity
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.LanguageChange
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.lang.Exception
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import javax.inject.Inject

class NLoginActivity : AppCompatActivity() {

    val TAG = "NLoginActivity"
    @Inject
    lateinit var userRepo: IUserRepo
    var binding: ActivityNloginBinding? = null
    val GOOGLE_SIGN_IN_CODE = 11011
    val FB_SIGN_IN_CODE = 12011
    val callbackManager: CallbackManager = CallbackManager.Factory.create()

    lateinit var viewModel: NLoginViewModel
    val compositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (application as MyApplication).appComponent?.inject(this)
        printhashkey()
        binding = DataBindingUtil.setContentView(this, R.layout.activity_nlogin)
        viewModel =
            ViewModelProviders.of(this, NLoginViewModelFactory(userRepo)).get(NLoginViewModel::class.java)
        LanguageChange.changeTo(viewModel.getLang(), this)
        viewModel.checkLogin().subscribe {
            it?.apiToken?.let {
                startActivity(Intent(this, MainActivity::class.java))
            }
        }
        binding?.go2Register?.setOnClickListener {
            startActivity(Intent(this, RegisterActivity::class.java))
        }
        binding?.googleSignin?.setOnClickListener {
            val googleSignInOption =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail()
                    .requestIdToken(Constants.SERVER_CLIENT_ID).build()

            val googleSignInClient = GoogleSignIn.getClient(this, googleSignInOption)
            startActivityForResult(googleSignInClient.signInIntent, GOOGLE_SIGN_IN_CODE)
        }
        binding?.viewModel = viewModel
        binding?.login?.setOnClickListener {
            it.visibility = View.GONE
            binding?.loader?.visibility = View.VISIBLE
            val user = User()

            binding?.email?.let {
                if (it.text.isNotEmpty()) {
                    user.email = it.text.toString()
                } else {
                    it.context.makeToast(resources.getString(R.string.invalid_email))
                    binding?.login?.visibility = View.VISIBLE
                    binding?.loader?.visibility = View.GONE
                    return@setOnClickListener
                }
            }


            binding?.password?.let {
                if (it.text.isNotEmpty()) {
                    user.password = it.text.toString()
                } else {
                    it.context.makeToast(resources.getString(R.string.password_required))
                    binding?.login?.visibility = View.VISIBLE
                    binding?.loader?.visibility = View.GONE
                    return@setOnClickListener
                }
            }


            compositeDisposable.add(viewModel.login(user).subscribe { response ->
                if (response.status == 1) {
                    response.apiToken?.let {
                        response.user?.apiToken = it
                        viewModel.saveUserData(response.user!!)
                        saveUserCredit(it)
                        this.makeToast(resources.getString(R.string.login_done))
                        startActivity(Intent(this, MainActivity::class.java))
                    } ?: this.makeToast(resources.getString(R.string.login_failed))
                } else {
                    this.makeToast(resources.getString(R.string.login_failed))
                    binding?.loader?.visibility = View.GONE
                    binding?.login?.visibility = View.VISIBLE
                }

            })
        }


        // start fb login

        binding?.loginButton?.setReadPermissions(listOf("email"))
        binding?.loginButton?.registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult?) {
                result?.let {
                    val credential = FacebookAuthProvider.getCredential(it.accessToken.token)
                    val userId = it.accessToken.userId
                    val auth = FirebaseAuth.getInstance()
                    val dialog = ProgressDialog.show(
                        this@NLoginActivity,
                        "Wait...",
                        "Wait, retrieving your login data",
                        false,
                        false
                    )
                    auth.signInWithCredential(credential).addOnCompleteListener {
                        if (it.isSuccessful) {
                            val user = User()
                            user.email = it.result?.user?.email
                            user.type = Constants.USER_TYPE_ANONYMOUS
                            viewModel.anonymousRegister(user, userId).subscribe({ response ->
                                response.apiToken?.let {
                                    user.apiToken = it
                                    user.type = response.type
                                    user.phone = response.user?.phone
                                    user.rating = response.user?.rating
                                    user.address = response.user?.address
                                    user.image = response.user?.image
                                    viewModel.saveUserData(user)
                                    saveUserCredit(it)
                                    startActivity(Intent(this@NLoginActivity, MainActivity::class.java))
                                    binding?.loginButton?.visibility = View.VISIBLE
                                    binding?.login?.visibility = View.VISIBLE
                                    binding?.googleSignin?.visibility = View.VISIBLE
                                    binding?.loader?.visibility = View.GONE
                                    makeToast(getString(R.string.login_done))
                                    dialog.dismiss()
                                }
                            }, {
                                Log.e(TAG, "An error occurred", it)
                                makeToast(resources.getString(R.string.login_failed))
                                dialog.dismiss()

                            })
                        } else {
                            makeToast(resources.getString(R.string.login_failed))
                        }
                    }.addOnFailureListener {
                        makeToast(resources.getString(R.string.login_failed))
                        Log.d(TAG, "an error occurred ", it)
                    }
                }
            }

            override fun onCancel() {
                Log.d(TAG, "Facebook Login cancelled")
            }

            override fun onError(error: FacebookException?) {
                Log.e(TAG, "An error occurred", error)
            }
        })


    }

    fun saveUserCredit(apToken: String) {
        if (this::viewModel.isInitialized) {
            viewModel.userCreditOnline(apToken).subscribe(object : Observer<UserCredit> {
                override fun onComplete() {
                    Log.d(TAG, "done getting user credits")
                }

                override fun onSubscribe(d: Disposable) {
                    compositeDisposable.add(d)
                }

                override fun onNext(t: UserCredit) {
                    viewModel.saveCredits(t)
                }

                override fun onError(e: Throwable) {
                    Log.e(TAG, "An error occurred ", e)
                }
            })
        } else {
            Log.e(TAG, "ViewModel hasn't initialized yet")
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        callbackManager.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GOOGLE_SIGN_IN_CODE) {
            val signInTask = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                signInTask.result?.email?.let {
                    val idToken = "${signInTask.result?.id ?: 0}"
                    val user = User()
                    user.email = it
                    user.type = Constants.USER_TYPE_ANONYMOUS
                    val dialog = ProgressDialog.show(
                        this@NLoginActivity,
                        "Wait...",
                        "Wait, retrieving your login data",
                        false,
                        false
                    )
                    viewModel.anonymousRegister(user, idToken).subscribe({ response ->
                        response.apiToken?.let {
                            user.apiToken = it
                            user.type = response.type
                            user.phone = response.user?.phone
                            user.rating = response.user?.rating
                            user.address = response.user?.address
                            user.image = response.user?.image
                            user.name = response.user?.name
                            viewModel.saveUserData(user)
                            saveUserCredit(it)
                            startActivity(Intent(this, MainActivity::class.java))
                            this.makeToast(getString(R.string.login_done))
                            dialog.dismiss()
                        } ?: run {
                            dialog.dismiss()
                            this.makeToast(getString(R.string.login_failed))
                        }
                    }, {
                        dialog.dismiss()
                        this.makeToast(getString(R.string.login_failed))
                        Log.e(TAG, "An error occurred ", it)
                    })

                }
            } catch (e: Exception) {
                this.makeToast(getString(R.string.login_failed))
                e.printStackTrace()

            }
        }
    }

    fun printhashkey() {

        try {
            val info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
