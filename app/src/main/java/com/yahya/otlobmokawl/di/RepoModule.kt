package com.yahya.otlobmokawl.di

import android.content.Context
import android.content.SharedPreferences
import com.yahya.otlobmokawl.data.network.AppApi
import com.yahya.otlobmokawl.data.repo.ClientRepo
import com.yahya.otlobmokawl.data.repo.ContractorRepo
import com.yahya.otlobmokawl.data.repo.ProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import dagger.Module
import dagger.Provides

@Module
class RepoModule(context: Context) {

    @Provides
    fun provideUserRepo(loginShared: SharedPreferences,appApi: AppApi): IUserRepo {
        return UserRepo(loginShared,appApi)
    }

    @Provides
    fun provideClientRepo(loginShared: SharedPreferences,appApi: AppApi): IClientRepo {
        return ClientRepo(appApi,loginShared)
    }

    @Provides
    fun provideContractorRepo(loginShared: SharedPreferences,appApi: AppApi): IContractorRepo {
        return ContractorRepo(appApi,loginShared)
    }

    @Provides
    fun provideProjectRepo(appApi: AppApi): IProjectRepo {
        return ProjectRepo(appApi)
    }
}