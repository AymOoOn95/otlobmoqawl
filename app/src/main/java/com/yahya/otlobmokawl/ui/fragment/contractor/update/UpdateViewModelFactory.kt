package com.yahya.otlobmokawl.ui.fragment.contractor.update

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class UpdateViewModelFactory(val userRepo: IUserRepo, val contractorRepo: IContractorRepo) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return UpdateContractorViewModel(userRepo, contractorRepo) as T
    }
}