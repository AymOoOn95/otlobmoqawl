package com.yahya.otlobmokawl.otlobmo2ol

import android.content.Context
import org.junit.Test

import org.junit.Assert.*
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(MockitoJUnitRunner::class)
class ExampleUnitTest {
    val testString = "hello testing"

    @Mock
    val context: Context? = null

    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun test_Strings() {
        context?.let {
            val string: String = it.resources.getString(R.string.test_string)
            assertSame(testString, string)
        } ?: assert(false)
    }
}