package com.yahya.otlobmokawl.ui.fragment.informative

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.InformativeFragmentLayoutBinding
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class InformativeFragment : Fragment() {

    @Inject
    lateinit var userRepo: IUserRepo

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity?.application as MyApplication).appComponent?.inject(this)
        val binding: InformativeFragmentLayoutBinding =
            DataBindingUtil.inflate(inflater, R.layout.informative_fragment_layout, container, false)
        val viewModel: InformativeViewModel =
            ViewModelProviders.of(this, InformativeViewModelFactory(userRepo)).get(InformativeViewModel::class.java)

        arguments?.getString(Constants.INFORMATIVE_ARGS_KEY)?.let {
            viewModel.getInformation(it).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    binding.information.text = Html.fromHtml(it.name)
                }
        }
        return binding.root
    }
}