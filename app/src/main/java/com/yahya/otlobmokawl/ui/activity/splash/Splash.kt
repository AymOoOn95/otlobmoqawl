package com.yahya.otlobmokawl.ui.activity.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.FrameLayout
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.ui.activity.nlogin.NLoginActivity
import com.yahya.otlobmokawl.ui.activity.userbanned.UserBannedActivity
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class Splash : AppCompatActivity() {
    @Inject
    lateinit var userRepo: IUserRepo

    val compositeDisposable = CompositeDisposable()
    val TAG = "Splash"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        (application as MyApplication).appComponent?.inject(this)

    }

    override fun onResume() {
        super.onResume()
        val viewModel: SplashViewModel =
            ViewModelProviders.of(this, SplashViewModelFactory(userRepo)).get(SplashViewModel::class.java)

        compositeDisposable.add(viewModel.userData().subscribe {
            it.apiToken?.let {
                viewModel.myCredits(it).subscribe(object : Observer<UserCredit> {
                    override fun onComplete() {
                        Log.d(TAG, "onComplete()")
                    }

                    override fun onSubscribe(d: Disposable) {
                        Log.d(TAG, "onComplete()")
                        compositeDisposable.add(d)
                    }

                    override fun onNext(it: UserCredit) {
                        viewModel.saveCredits(it)
                        if (it.status == Constants.STATUS_BANNED) {
                            startActivity(Intent(this@Splash, UserBannedActivity::class.java))
                            compositeDisposable.dispose()
                            finish()
                        }
                        findViewById<FrameLayout>(R.id.splash).setOnClickListener {
                            startActivity(Intent(this@Splash, NLoginActivity::class.java))
                        }
                    }

                    override fun onError(e: Throwable) {
                        Log.e(TAG, " an error occurred ${e.message}")
                        viewModel.saveCredits(UserCredit("", 0, 0, 0))
                        findViewById<FrameLayout>(R.id.splash).setOnClickListener {
                            startActivity(Intent(this@Splash, NLoginActivity::class.java))
                        }

                    }

                })

            }
        })

        compositeDisposable.add(Observable.timer(3, TimeUnit.SECONDS).subscribe {
            startActivity(Intent(this, NLoginActivity::class.java))
            finish()

        })

    }

    override fun onPause() {
        super.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
