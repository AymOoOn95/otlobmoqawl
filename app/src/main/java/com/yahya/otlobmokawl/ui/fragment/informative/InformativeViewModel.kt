package com.yahya.otlobmokawl.ui.fragment.informative

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.network.response.InformativeResponse
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.util.Constants
import io.reactivex.Observable

class InformativeViewModel(val userRepo: IUserRepo) : ViewModel() {

    fun getInformation(type: String): Observable<InformativeResponse> {
        return if (type == Constants.PRIVACY_ARGS_VALUE) {
            (userRepo as UserRepo).privacyPolicy()
        } else {
            (userRepo as UserRepo).about()
        }
    }
}