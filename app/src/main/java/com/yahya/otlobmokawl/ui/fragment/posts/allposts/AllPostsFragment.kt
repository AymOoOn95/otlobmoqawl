package com.yahya.otlobmokawl.ui.fragment.posts.allposts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.AllPostsFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.AllPostsAdapter
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class AllPostsFragment : Fragment() {

    @Inject
    lateinit var userRepo: IUserRepo
    @Inject
    lateinit var projectRepo: IProjectRepo

    var compositeDisposable = CompositeDisposable()

    var binding: AllPostsFragmentBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.all_posts_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        var categoryId = arguments?.getInt(Constants.CATEGORY_ID_ARGUMENT_KEY) ?: 1

        val viewModel: AllPostsViewModel = ViewModelProviders.of(this, AllPostsViewModelFactory(projectRepo, userRepo))
            .get(AllPostsViewModel::class.java)
        viewModel.getUserData().subscribe(object : Observer<User> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onNext(t: User) {
                t.apiToken?.let { apiToken ->
                    viewModel.getPosts(apiToken, categoryId).subscribe {
                        binding?.loader?.visibility = View.GONE
                        if (it.isEmpty()) {
                            binding?.noItem?.visibility = View.VISIBLE
                        } else {
                            binding?.postsRecycler?.visibility = View.VISIBLE
                            binding?.postsRecycler?.adapter =
                                AllPostsAdapter(
                                    it,
                                    context,
                                    arguments?.getString(Constants.CATEGORY_IMAGE_ARGU_KEY),
                                    object : AllPostsAdapter.OnFavButtonClickListener {
                                        override fun onClick(post: Post, faveBtn: ImageView, position: Int) {
                                            compositeDisposable.add(
                                                viewModel.addPost2Fav(
                                                    post.id!!,
                                                    apiToken
                                                ).subscribe {
                                                    if (it.status == 1) {
                                                        if (it.isDeleted == 1) {
                                                            context?.makeToast(getString(R.string.deleted_from_fav))
                                                            faveBtn.setImageResource(R.drawable.ic_empty_fav_primary)
                                                        } else {
                                                            context?.makeToast(getString(R.string.added_2_fav))
                                                            faveBtn.setImageResource(R.drawable.ic_fav_filled)
                                                        }
                                                    }
                                                })
                                        }
                                    })
                            binding?.postsRecycler?.layoutManager = LinearLayoutManager(context)
                        }
                    }
                }
            }

            override fun onError(e: Throwable) {
                context?.makeToast(getString(R.string.login_first))
            }
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}