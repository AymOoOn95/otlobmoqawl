package com.yahya.otlobmokawl.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.PurchaseItem

class PurchasesAdapter(val purchaseItems: ArrayList<PurchaseItem>) :
    RecyclerView.Adapter<PurchasesAdapter.PurchaseViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PurchaseViewHolder =
        PurchaseViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.purchase_item_layout, parent, false))

    override fun getItemCount(): Int = purchaseItems.size

    override fun onBindViewHolder(holder: PurchaseViewHolder, position: Int) {
        if (position % 2 == 1) {
            holder.item.setBackgroundColor(holder.item.context.getColor(R.color.header_grey))
        }
        holder.purchaseDate.text = purchaseItems[position].purchaseDate.split(" ")[0]
        holder.purchaseName.text = purchaseItems[position].name
        holder.purchasePrice.text = purchaseItems[position].price.toString()
    }

    class PurchaseViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val item: LinearLayout = itemView.findViewById(R.id.purchase_item)
        val purchaseName: TextView = itemView.findViewById(R.id.purchase_name)
        val purchaseDate: TextView = itemView.findViewById(R.id.purchase_date)
        val purchasePrice: TextView = itemView.findViewById(R.id.purchase_price)
    }
}