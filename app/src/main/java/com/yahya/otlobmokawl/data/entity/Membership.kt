package com.yahya.otlobmokawl.data.entity

import com.google.gson.annotations.SerializedName

class Membership(
    val id: Int,
    val code: String,
    val type: String,
    @SerializedName("name")
    val name: String,
    @SerializedName("desc")
    val desc: String,
    val cost: Int,
    @SerializedName("view_posts")
    val viewPosts: Int,
    @SerializedName("create_posts")
    val createPosts: Int,
    @SerializedName("create_bids")
    val createBids: Int

)
