package com.yahya.otlobmokawl.ui.fragment.contractor.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class ContractorViewModelFactory(val contractorRepo:IContractorRepo,val userRepo:IUserRepo) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ContractorViewModel(contractorRepo,userRepo) as T
    }
}