package com.yahya.otlobmokawl.ui.fragment.posts.add

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class AddViewModelFactory(val projectRepo: IProjectRepo, val userRepo: IUserRepo):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AddViewModel(projectRepo,userRepo) as T
    }
}