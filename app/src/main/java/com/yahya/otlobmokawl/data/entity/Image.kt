package com.yahya.otlobmokawl.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Image(var id:Int?,
            @SerializedName("post_id")
            var postId:Int?,
            var image:String?,
            var status:Int? ) : Parcelable