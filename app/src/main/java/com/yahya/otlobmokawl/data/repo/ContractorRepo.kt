package com.yahya.otlobmokawl.data.repo

import android.content.SharedPreferences
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Contractor
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.network.AppApi
import com.yahya.otlobmokawl.data.network.response.AddBidResponse
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.util.Constants
import io.reactivex.Observable
import okhttp3.MultipartBody


class ContractorRepo(val appApi: AppApi, val loginShared: SharedPreferences) : IContractorRepo {
    override fun contractorRegister(
        contractor: Contractor,
        images: ArrayList<MultipartBody.Part?>
    ): Observable<RegisterResponse> {
        var response = appApi.register(
            null,
            contractor.email,
            contractor.phone,
            contractor.name,
            contractor.password,
            Constants.USER_TYPE_CONTRACTOR,
            contractor.sid,
            contractor.areaId,
            contractor.address,
            contractor.about,
            contractor.jobTitle,
            images
        )
        contractor.apiToken?.let {
            response = appApi.completeRegister(
                contractor.apiToken,
                contractor.email,
                contractor.phone,
                contractor.name,
                contractor.password,
                Constants.USER_TYPE_CONTRACTOR,
                contractor.sid,
                contractor.areaId,
                contractor.address,
                contractor.about,
                contractor.jobTitle,
                images
            )
        }
        return response
    }

    override fun makeBid(bid: Bid, apiToken: String): Observable<AddBidResponse> {
        return appApi.makeOffer(bid.postId!!, apiToken, bid.bid!!)
    }

    override fun removeBid(bid: Bid) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun askQuestion(question: Question, apiToken: String, postId: Int): Observable<RegisterResponse> {
        return appApi.askQuestion(postId, apiToken, question.question!!)
    }

    override fun uploadImage(project: Post, images: List<String>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateContractor(
        contractor: Contractor,
        newPassword: String?,
        imagePart: MultipartBody.Part?
    ): Observable<RegisterResponse> {
        return appApi.updateAccount(
            contractor.apiToken,
            contractor.password,
            newPassword,
            contractor.name,
            contractor.areaId,
            contractor.address,
            contractor.phone,
            contractor.jobTitle,
            contractor.about,
            imagePart
        )
    }

    override fun getContractorProjects(contractor: Contractor): Observable<List<Post>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}