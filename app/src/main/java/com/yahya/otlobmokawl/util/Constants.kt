package com.yahya.otlobmokawl.util

class Constants {
    companion object {
        const val LOGIN_SHARED_FILE_NAME = "login"
        const val LOGIN_SHARED_EMAIL_KEY = "email"
        const val LOGIN_SHARED_ID_KEY = "id"
        const val LOGIN_SHARED_NAME_KEY = "name"
        const val LOGIN_SHARED_PHONE_KEY = "phone"
        const val LOGIN_SHARED_API_TOKEN_KEY = "api_token"
        const val LOGIN_SHARED_RATING_KEY = "rating"
        const val LOGIN_SHARED_LANG_KEY = "lang"
        const val LOGIN_SHARED_TYPE_KEY = "type"
        const val LOGIN_SHARED_IMAGE_KEY = "user_Image"
        const val LOGIN_SHARED_STATUS_KEY = "status"
        const val API_URL = "http://otlobmokawl.com/api/"
        const val USER_TYPE_CLIENT = "c"
        const val USER_TYPE_CONTRACTOR = "o"
        const val USER_TYPE_ANONYMOUS = "a"
        const val CONTRACTOR_TYPE_INDIVIDUAL = "individual"
        const val CONTRACTOR_TYPE_COMPANY = "company"
        const val CATEGORY_ID_ARGUMENT_KEY = "category_id"
        const val CATEGORY_IMAGE_ARGU_KEY = "cate_image"
        const val POST_ARGU_KEY = "post"
        const val API_TOKEN_ARGU_KEY = "apiToken"
        const val LANG_AR = "ar"
        const val LANG_EN = "en"
        const val CREDIT_TEST_ITEM_SKU = "con50"
        const val SERVER_CLIENT_ID = "959668035115-t2u1ju5eisvkoc8qq79d95425gk5eptv.apps.googleusercontent.com"

        const val CLIENT_VIP_SKU = "client_vip"
        const val CLIENT_BASIC_SKU = "basic"
        const val CLIENT_MEDIUM_SKU = "client_silver"
        const val CLIENT_EXTREM_SKU = "client_golden"

        const val CONTRACTOR_VIP_SKU = "contractor_vip"
        const val CONTRACTOR_BASIC_SKU = "contractor_bronze"
        const val CONTRACTOR_MEDIUM_SKU = "contractor_silver"
        const val CONTRACTOR_EXTREM_SKU = "contractor_golden"

        const val USER_CREDIT_STATUS_KEY = "status"
        const val USER_CREDIT_POSTS_KEY = "posts"
        const val USER_CREDIT_BIDS_KEY = "bids"

        const val PRIVACY_ARGS_VALUE = "privacy"
        const val ABOUT_ARGS_VALUE = "about"
        const val INFORMATIVE_ARGS_KEY = "type"

        const val STATUS_FAILED = 0
        const val STATUS_SUCCESS = 1
        const val STATUS_BANNED = 2


        // otlobmokawlauth@gmail.com

    }
}