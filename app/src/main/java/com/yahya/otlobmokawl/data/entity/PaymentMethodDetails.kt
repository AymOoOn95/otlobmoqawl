package com.yahya.otlobmokawl.data.entity

import com.google.gson.annotations.SerializedName

class PaymentMethodDetails(
    var id: Int?,
    var name: String?,
    @SerializedName("method_id")
    var methodId: Int?
) {
    override fun toString(): String {
        return this.name ?: "Object has no name"
    }
}