package com.yahya.otlobmokawl.ui.activity.initialregister

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class InitialRegisterViewModelFactory(val userRepo: IUserRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InitialRegesterViewModel(userRepo) as T
    }
}