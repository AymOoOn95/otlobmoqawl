package com.yahya.otlobmokawl.ui.fragment.home

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Category
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class HomeViewModel(val projectRepo: IProjectRepo, val userRepo: IUserRepo) : ViewModel() {

    fun categories(): Observable<ArrayList<Category>> {
        return projectRepo.getCategories(null, userRepo.getLanguage()).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }
}