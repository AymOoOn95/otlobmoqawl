package com.yahya.otlobmokawl.ui.fragment.client.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class ClientRegisterViewModelFactory(val userRepo:IUserRepo,val clientRepo: IClientRepo) : ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return ClientRegisterViewModel(userRepo,clientRepo) as T
    }
}