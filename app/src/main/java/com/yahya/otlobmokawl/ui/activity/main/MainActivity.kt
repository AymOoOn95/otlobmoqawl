package com.yahya.otlobmokawl.ui.activity.main

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.yahya.otlobmokawl.R
import android.content.pm.PackageManager
import android.util.Base64
import android.util.Log
import android.view.View
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.ui.fragment.home.HomeFragment
import com.yahya.otlobmokawl.ui.fragment.posts.add.AddFragment
import com.yahya.otlobmokawl.ui.fragment.question.QuetionsFragment
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.LabelVisibilityMode
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.ui.activity.register.RegisterActivity
import com.yahya.otlobmokawl.ui.fragment.account.AccountFragment
import com.yahya.otlobmokawl.ui.fragment.posts.favourites.FavouritesFragment
import com.yahya.otlobmokawl.ui.fragment.posts.myposts.MyPostsFragment
import com.yahya.otlobmokawl.util.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var userRepo: IUserRepo

    var currentUser: User? = null

    val compositeDisposable = CompositeDisposable()

    lateinit var bottomNavigation: BottomNavigationView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        printhashkey()
        (application as MyApplication).appComponent?.inject(this)
        val viewModel: MainViewModel =
            ViewModelProviders.of(this, MainViewModelFactory(userRepo)).get(MainViewModel::class.java)
        LanguageChange.changeTo(viewModel.getLang(), this)
        setContentView(R.layout.activity_main)
        bottomNavigation = findViewById(R.id.bottom_navigation)
        bottomNavigation.labelVisibilityMode = LabelVisibilityMode.LABEL_VISIBILITY_LABELED
        supportFragmentManager.transaction {
            replace(R.id.fragment_container, HomeFragment())
        }

        bottomNavigation.setOnNavigationItemSelectedListener {
            var fragment: Fragment? = null
            when (it.itemId) {
                R.id.my_list -> {
                    bottomNavigation.menu.findItem(R.id.fav).setIcon(R.drawable.ic_fav_empty)
                    fragment = MyPostsFragment()
                }
                R.id.main_menu -> {
                    bottomNavigation.menu.findItem(R.id.fav).setIcon(R.drawable.ic_fav_empty)
                    fragment = HomeFragment()
                }
                R.id.fav -> {
                    it.setIcon(R.drawable.ic_fav_filled)
                    fragment = FavouritesFragment()
                }
                R.id.questions -> {
                    bottomNavigation.menu.findItem(R.id.fav).setIcon(R.drawable.ic_fav_empty)
                    fragment = QuetionsFragment()
                }


                R.id.account -> {
                    if (this.checkInternetConnectivity()) {
                        compositeDisposable.add(viewModel.userData().subscribe { user ->
                            user.apiToken?.let {
                                bottomNavigation.menu.findItem(R.id.fav).setIcon(R.drawable.ic_fav_empty)
                                if (user.type == Constants.USER_TYPE_CLIENT || user.type == Constants.USER_TYPE_CONTRACTOR) {
                                    supportFragmentManager.transaction {
                                        replace(R.id.fragment_container, AccountFragment())
                                    }
                                } else {
                                    startActivity(Intent(this, RegisterActivity::class.java))
                                }
                            } ?: this.makeToast(getString(R.string.login_first))
                        })
                    } else {
                        this.makeToast(resources.getString(R.string.no_internet_connectivity))
                    }
                }
            }
            if (this.checkInternetConnectivity()) {
                fragment?.let {
                    supportFragmentManager.transaction {
                        replace(R.id.fragment_container, it)
                    }
                }
            } else {
                this.makeToast(resources.getString(R.string.no_internet_connectivity))
            }
            return@setOnNavigationItemSelectedListener true
        }

        bottomNavigation.selectedItemId = R.id.main_menu

        compositeDisposable.add(viewModel.userData().subscribe {
            currentUser = it
//            if (it.type == Constants.USER_TYPE_CLIENT || it.type == Constants.USER_TYPE_ANONYMOUS) {
//                findViewById<android.widget.Button>(R.id.add_new).visibility = View.VISIBLE
//            } else {
//                findViewById<android.widget.Button>(R.id.add_new).visibility = View.GONE
//            }
        })

        findViewById<Button>(R.id.add_new).setOnClickListener {
            if (this.checkInternetConnectivity()) {
                currentUser?.type?.let {
                    if (it.equals(Constants.USER_TYPE_ANONYMOUS)) {
                        startActivity(Intent(this, RegisterActivity::class.java))
                    } else {
                        currentUser?.apiToken?.let {
                            compositeDisposable.add(viewModel.myCredits().subscribe {
                                when (it.status) {
                                    Constants.STATUS_SUCCESS -> {
                                        if (it.posts > 0) {
                                            supportFragmentManager.transaction {
                                                replace(R.id.fragment_container, AddFragment())
                                            }
                                        } else {
                                            this.makeToast(getString(R.string.not_enough_credit))
                                        }
                                    }
                                    Constants.STATUS_FAILED -> {
                                        this.makeToast(getString(R.string.not_permitted))
                                    }
                                    Constants.STATUS_BANNED -> {
                                        this.makeToast(getString(R.string.user_banned))
                                    }
                                }
                            })
                        }
                    }

                }
            } else {
                this.makeToast(resources.getString(R.string.no_internet_connectivity))
            }
        }


    }

    override fun onBackPressed() {
        Log.d("MainActivity", "backPressed()")
        // here control add btn
        currentUser?.type?.let {
            if (it == Constants.USER_TYPE_CLIENT) {
                findViewById<Button>(R.id.add_new).visibility = View.VISIBLE
            }
        }
        supportFragmentManager.transaction {
            replace(R.id.fragment_container, HomeFragment())
        }
        bottomNavigation.selectedItemId = R.id.main_menu

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }


    fun printhashkey() {

        try {
            val info = packageManager.getPackageInfo(
                packageName,
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {

        } catch (e: NoSuchAlgorithmException) {

        }

    }
}
