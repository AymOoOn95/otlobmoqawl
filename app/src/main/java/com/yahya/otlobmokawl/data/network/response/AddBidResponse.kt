package com.yahya.otlobmokawl.data.network.response

import com.google.gson.annotations.SerializedName
import com.yahya.otlobmokawl.data.entity.Bid

data class AddBidResponse(
    val status: Int = 1,
    val bid: Bid,
    @SerializedName("available_bids")
    val availableBids: Int
)