package com.yahya.otlobmokawl.data.repo

import android.content.SharedPreferences
import androidx.core.content.edit
import com.yahya.otlobmokawl.data.entity.*
import com.yahya.otlobmokawl.data.network.AppApi
import com.yahya.otlobmokawl.data.network.response.*
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.util.Constants
import io.reactivex.Observable

class UserRepo(val loginShared: SharedPreferences, val appApi: AppApi) : IUserRepo {
    override fun register(user: User, idToken: String): Observable<RegisterResponse> {
        if (idToken == "") {
            return appApi.register(
                idToken,
                user.email,
                null,
                user.name,
                user.password,
                user.type,
                null,
                user.areaId,
                user.address,
                null,
                null,
                null
            )
        } else {
            return appApi.register(
                idToken,
                user.email,
                user.phone,
                user.name,
                user.password,
                user.type,
                null,
                user.areaId,
                user.address,
                null,
                null,
                null
            )
        }
    }

    override fun login(user: User): Observable<RegisterResponse> {
        return appApi.login(user.email, "dummy", user.password)
    }

    override fun logout() {
        loginShared.edit {
            clear()
        }
    }

    override fun saveUserData(user: User) {
        loginShared.edit {
            putString(Constants.LOGIN_SHARED_NAME_KEY, user.name)
            putString(Constants.LOGIN_SHARED_EMAIL_KEY, user.email)
            putString(Constants.LOGIN_SHARED_API_TOKEN_KEY, user.apiToken)
            putString(Constants.LOGIN_SHARED_PHONE_KEY, user.phone)
            putString(Constants.LOGIN_SHARED_TYPE_KEY, user.type)
            putInt(Constants.LOGIN_SHARED_RATING_KEY, user.rating ?: -1)
            putString(Constants.LOGIN_SHARED_IMAGE_KEY, user.image)
        }
    }

    override fun getUserData(): Observable<User> {
        val user = User(
            loginShared.getString(Constants.LOGIN_SHARED_NAME_KEY, null),
            loginShared.getString(Constants.LOGIN_SHARED_EMAIL_KEY, null),
            null,
            loginShared.getString(Constants.LOGIN_SHARED_PHONE_KEY, null),
            loginShared.getString(Constants.LOGIN_SHARED_API_TOKEN_KEY, null),
            loginShared.getInt(Constants.LOGIN_SHARED_RATING_KEY, -1),
            loginShared.getString(Constants.LOGIN_SHARED_IMAGE_KEY, null),
            loginShared.getString(Constants.LOGIN_SHARED_TYPE_KEY, null),
            null,
            null,
            null
        )

        return Observable.just(user)
    }

    override fun updateUserData(user: User): Observable<User> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun getLocations(parentId: Int?): Observable<ArrayList<Address>> {
        return appApi.getLocations(parentId)
    }

    override fun changeLanguage(lang: String) {
        loginShared.edit {
            putString(Constants.LOGIN_SHARED_LANG_KEY, lang)
        }
    }

    override fun getLanguage(): String {
        return loginShared.getString(Constants.LOGIN_SHARED_LANG_KEY, Constants.LANG_AR)
    }

    override fun getUserDataOnline(apiToken: String): Observable<UserDataResponse> {
        return this.appApi.userData(apiToken)
    }

    fun paymentMethods(): Observable<ArrayList<PaymentMethods>> = appApi.paymentMethods(this.getLanguage())

    override fun saveCredit(userCredit: UserCredit) {
        loginShared.edit {
            putInt(Constants.USER_CREDIT_STATUS_KEY, userCredit.status ?: 0)
            putInt(Constants.USER_CREDIT_POSTS_KEY, userCredit.posts ?: 0)
            putInt(Constants.USER_CREDIT_BIDS_KEY, userCredit.bids ?: 0)
        }
    }

    override fun myCreditsFromShared(): Observable<UserCredit> {
        val userCredit = UserCredit(
            null,
            loginShared.getInt(Constants.USER_CREDIT_STATUS_KEY, 0),
            loginShared.getInt(Constants.USER_CREDIT_POSTS_KEY, 0),
            loginShared.getInt(Constants.USER_CREDIT_BIDS_KEY, 0)
        )
        return Observable.just(userCredit)
    }

    override fun myCredits(apiToken: String): Observable<UserCredit> {
        return appApi.myCredits(apiToken)
    }

    override fun purchasesReport(apiToken: String): Observable<PurchaseReportResponse> {
        return appApi.purchaseItems(apiToken, getLanguage())
    }

    override fun makePurchase(
        apiToken: String,
        sku: String,
        orderId: String,
        purchaseToken: String,
        signature: String,
        purchaseTime: String,
        price: String,
        currencyCode: String
    ): Observable<BuyCreditResponse> {
        return appApi.makePurchase(
            apiToken,
            5,
            sku,
            orderId,
            purchaseToken,
            signature,
            purchaseTime,
            price,
            currencyCode
        )
    }

    fun privacyPolicy(): Observable<InformativeResponse> {
        return appApi.privacyPolicy(getLanguage())
    }

    fun about(): Observable<InformativeResponse> {
        return appApi.about(getLanguage())
    }

    override fun memberships(): Observable<ArrayList<Membership>> {
        return appApi.memberships(getLanguage())
    }
}