package com.yahya.otlobmokawl.ui.activity.ads.done

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.yahya.otlobmokawl.R

class DoneAdding : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_done_adding)
    }
}
