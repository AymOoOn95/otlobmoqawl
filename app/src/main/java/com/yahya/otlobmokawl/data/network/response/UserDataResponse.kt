package com.yahya.otlobmokawl.data.network.response

import com.google.gson.annotations.SerializedName
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.entity.Contractor

data class UserDataResponse(
    val client: Client?,
    val contractor: Contractor?,
    val email: String?,
    val id: Int?,
    val image: String?,
    @SerializedName("is_approved")
    val isApproved: Int?,
    @SerializedName("membership_id")
    val membershipId: Int?,
    val name: String?,
    @SerializedName("phone_number")
    val phoneNumber: String?,
    val rating: Int?,
    val type: String?,
    @SerializedName("created_at")
    val createdAt: String?,
    @SerializedName("updated_at")
    val updatedAt: String?
)