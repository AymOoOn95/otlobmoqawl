package com.yahya.otlobmokawl.di

import com.yahya.otlobmokawl.ui.activity.credit.MyCreditActivity
import com.yahya.otlobmokawl.ui.activity.initialregister.InitialRegisterActivity
import com.yahya.otlobmokawl.ui.activity.login.LoginActivity
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.ui.activity.nlogin.NLoginActivity
import com.yahya.otlobmokawl.ui.activity.register.RegisterActivity
import com.yahya.otlobmokawl.ui.activity.splash.Splash
import com.yahya.otlobmokawl.ui.fragment.account.AccountFragment
import com.yahya.otlobmokawl.ui.fragment.bid.view.PostBidFragment
import com.yahya.otlobmokawl.ui.fragment.client.register.ClientRegisterFragment
import com.yahya.otlobmokawl.ui.fragment.client.update.ClientUpdateFragment
import com.yahya.otlobmokawl.ui.fragment.contractor.register.ContractorRegisterFragment
import com.yahya.otlobmokawl.ui.fragment.contractor.update.UpdateContractorFragment
import com.yahya.otlobmokawl.ui.fragment.home.HomeFragment
import com.yahya.otlobmokawl.ui.fragment.informative.InformativeFragment
import com.yahya.otlobmokawl.ui.fragment.memberships.MembershipsFragment
import com.yahya.otlobmokawl.ui.fragment.paymentmethods.PaymentMethods
import com.yahya.otlobmokawl.ui.fragment.posts.add.AddFragment
import com.yahya.otlobmokawl.ui.fragment.posts.allposts.AllPostsFragment
import com.yahya.otlobmokawl.ui.fragment.posts.details.PostDetailsFragment
import com.yahya.otlobmokawl.ui.fragment.posts.favourites.FavouritesFragment
import com.yahya.otlobmokawl.ui.fragment.posts.myposts.MyPostsFragment
import com.yahya.otlobmokawl.ui.fragment.purchase.PurchaseReportFragment
import com.yahya.otlobmokawl.ui.fragment.question.QuetionsFragment
import dagger.Component

@Component(modules = [LocalStorageModule::class, RepoModule::class, NetworkModule::class])
interface AppComponent {

    fun inject(loginActivity: LoginActivity)
    fun inject(mainActivity: MainActivity)
    fun inject(clientRegisterFragment: ClientRegisterFragment)
    fun inject(contractorRegisterFragment: ContractorRegisterFragment)
    fun inject(nLoginActivity: NLoginActivity)
    fun inject(homeFragment: HomeFragment)
    fun inject(allPostsFragment: AllPostsFragment)
    fun inject(addFragment: AddFragment)
    fun inject(postDetailsFragment: PostDetailsFragment)
    fun inject(myPostsFragment: MyPostsFragment)
    fun inject(favouritesFragment: FavouritesFragment)
    fun inject(quetionsFragment: QuetionsFragment)
    fun inject(accountFragment: AccountFragment)
    fun inject(postBidFragment: PostBidFragment)
    fun inject(clientUpdateFragment: ClientUpdateFragment)
    fun inject(updateContractorFragment: UpdateContractorFragment)
    fun inject(paymentMethods: PaymentMethods)
    fun inject(splash: Splash)
    fun inject(myCreditActivity: MyCreditActivity)
    fun inject(purchaseReportFragment: PurchaseReportFragment)
    fun inject(initialRegisterActivity: InitialRegisterActivity)
    fun inject(membershipsFragment: MembershipsFragment)
    fun inject(informativeFragment: InformativeFragment)
    fun inject(registerActivity: RegisterActivity)
}