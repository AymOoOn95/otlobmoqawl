package com.yahya.otlobmokawl.ui.fragment.posts.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Category
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.ActivityAddAdsBinding
import com.yahya.otlobmokawl.ui.adapter.ImagesAdapter
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class AddFragment : Fragment() {

    @Inject
    lateinit var userRepo: IUserRepo
    @Inject
    lateinit var projectRepo: IProjectRepo

    val compositeDisposable = CompositeDisposable()

    var binding: ActivityAddAdsBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.activity_add_ads, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        activity?.findViewById<Button>(R.id.add_new)?.visibility = View.GONE
        val viewModel: AddViewModel =
            ViewModelProviders.of(this, AddViewModelFactory(projectRepo, userRepo)).get(AddViewModel::class.java)
        compositeDisposable.add(viewModel.userData().subscribe {
            var isContractor = "0"
            if (it.type == Constants.USER_TYPE_CONTRACTOR) {
                isContractor = "1"
            }
            it.apiToken?.let { apiToken ->
                binding?.let { nonNullableBinding ->
                    nonNullableBinding.imageRecycler.adapter = ImagesAdapter(context!!)
                    nonNullableBinding.imageRecycler.layoutManager = GridLayoutManager(context, 4)
                    var selectedSubCategoryId = -1
                    var selectedCategory = -1
                    compositeDisposable.add(viewModel.categories().subscribe {
                        it.add(0, Category(-1, -1, -1, -1, "", "اختر القسم"))
                        nonNullableBinding.categorySpinner.adapter =
                            ArrayAdapter<Category>(context, R.layout.support_simple_spinner_dropdown_item, it)

                        nonNullableBinding.categorySpinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {

                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    if (position > 0) {
                                        selectedCategory = (parent?.selectedItem as Category).id ?: -1
                                        compositeDisposable.add(viewModel.categories((parent?.selectedItem as Category).id).subscribe {
                                            nonNullableBinding.subcategorySpinner.adapter = ArrayAdapter<Category>(
                                                context,
                                                R.layout.support_simple_spinner_dropdown_item,
                                                it
                                            )

                                            nonNullableBinding.subcategorySpinner.onItemSelectedListener =
                                                object : AdapterView.OnItemSelectedListener {
                                                    override fun onNothingSelected(parent: AdapterView<*>?) {

                                                    }

                                                    override fun onItemSelected(
                                                        parent: AdapterView<*>?,
                                                        view: View?,
                                                        position: Int,
                                                        id: Long
                                                    ) {
                                                        selectedSubCategoryId =
                                                            (parent?.selectedItem as Category).id ?: -1
                                                    }
                                                }
                                        })
                                    }
                                }
                            }
                    })

                    var selectedAreaId = -1
                    compositeDisposable.add(viewModel.locations(null).subscribe {
                        it.add(0, Address(-1, -1, "اختر البلد"))
                        nonNullableBinding.countrySpinner.adapter =
                            ArrayAdapter<Address>(context, R.layout.support_simple_spinner_dropdown_item, it)
                        nonNullableBinding.countrySpinner.onItemSelectedListener =
                            object : AdapterView.OnItemSelectedListener {
                                override fun onNothingSelected(parent: AdapterView<*>?) {

                                }

                                override fun onItemSelected(
                                    parent: AdapterView<*>?,
                                    view: View?,
                                    position: Int,
                                    id: Long
                                ) {
                                    if (position > 0) {
                                        compositeDisposable.add(viewModel.locations((parent?.selectedItem as Address).id).subscribe {
                                            nonNullableBinding.areaSpinner.adapter = ArrayAdapter<Address>(
                                                context,
                                                R.layout.support_simple_spinner_dropdown_item,
                                                it
                                            )
                                            nonNullableBinding.areaSpinner.onItemSelectedListener =
                                                object : AdapterView.OnItemSelectedListener {
                                                    override fun onNothingSelected(parent: AdapterView<*>?) {

                                                    }

                                                    override fun onItemSelected(
                                                        parent: AdapterView<*>?,
                                                        view: View?,
                                                        position: Int,
                                                        id: Long
                                                    ) {
                                                        selectedAreaId = (parent?.selectedItem as Address).id ?: -1
                                                    }
                                                }

                                        })
                                    }
                                }
                            }
                    })
                    nonNullableBinding.addPost.setOnClickListener {
                        if (nonNullableBinding.title.text.isNotEmpty() && nonNullableBinding.postContent.text.isNotEmpty() && selectedAreaId != -1 && selectedSubCategoryId != -1 && nonNullableBinding.address.text.isNotEmpty()) {
                            nonNullableBinding.loader.visibility = View.VISIBLE
                            var post = Post(
                                null,
                                null,
                                nonNullableBinding.title.text.toString(),
                                selectedCategory,
                                selectedSubCategoryId,
                                selectedAreaId,
                                nonNullableBinding.address.text.toString(),
                                nonNullableBinding.postContent.text.toString(),
                                null,
                                null,
                                null,
                                null,
                                null, null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null,
                                null, null, null, null, null
                            )

                            var imagesParts =
                                (nonNullableBinding.imageRecycler.adapter as ImagesAdapter).getImagesAsFiles()

                            compositeDisposable.add(
                                viewModel.addPost(
                                    post,
                                    apiToken,
                                    isContractor,
                                    imagesParts
                                ).subscribe {
                                    var msg = getString(R.string.error_adding_post)
                                    if (it.status == 1) {
                                        msg = getString(R.string.done_adding_post)
                                        var postCreditStatus = 0
                                        if (it.availablePosts > 0) {
                                            postCreditStatus = 1
                                        }
                                        viewModel.updateUserCredit(
                                            UserCredit(
                                                null,
                                                postCreditStatus,
                                                it.availablePosts,
                                                0
                                            )
                                        )
                                        (context as AppCompatActivity).supportFragmentManager.transaction {
                                            replace(R.id.fragment_container, DoneFragment())
                                        }
                                    }

                                    context?.makeToast(msg)

                                })

                        } else {
                            context?.makeToast(getString(R.string.fill_all_fields))
                        }
                    }
                }
            } ?: context?.makeToast(getString(R.string.login_first))
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
        activity?.findViewById<Button>(R.id.add_new)?.visibility = View.VISIBLE

    }
}