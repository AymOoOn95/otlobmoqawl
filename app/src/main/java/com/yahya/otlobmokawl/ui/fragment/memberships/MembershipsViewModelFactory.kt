package com.yahya.otlobmokawl.ui.fragment.memberships

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class MembershipsViewModelFactory(val repo: IUserRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MembershipsViewModel(repo) as T
    }
}