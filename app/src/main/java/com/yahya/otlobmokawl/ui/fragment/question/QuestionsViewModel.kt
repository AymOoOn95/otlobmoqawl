package com.yahya.otlobmokawl.ui.fragment.question

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class QuestionsViewModel(val userRepo: IUserRepo, val clientRepo: IClientRepo) : ViewModel() {
    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun questions4Me(apiToken: String): Observable<ArrayList<Question>> {
        return clientRepo.questions4Me(apiToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun answerQuestion(question: Question, apiToken: String): Observable<Question> {
        return clientRepo.answerQuestion(question, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}