package com.yahya.otlobmokawl.ui.fragment.home

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Category
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.HomeFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.CategoryAdapter
import com.yahya.otlobmokawl.ui.adapter.FeaturedCategoryAdapter
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class HomeFragment : Fragment() {

    val TAG = "HomeFragment"

    @Inject
    lateinit var projectRepo: IProjectRepo
    @Inject
    lateinit var userRepo: IUserRepo

    var compositeDisposable = CompositeDisposable()
    var binding: HomeFragmentBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: HomeViewModel =
            ViewModelProviders.of(this, HomeViewModelFactory(projectRepo, userRepo)).get(HomeViewModel::class.java)
        binding?.tryAgain?.setOnClickListener {
            binding?.tryAgain?.visibility = View.GONE
            binding?.notFoundMsg?.visibility = View.GONE
            binding?.loader?.visibility = View.VISIBLE
            loadCategories(viewModel)
        }
        loadCategories(viewModel)

//        compositeDisposable.add(viewModel.userData().subscribe {
//            if (it.type == Constants.USER_TYPE_CLIENT || it.type == Constants.USER_TYPE_ANONYMOUS) {
//                activity?.findViewById<FloatingActionButton>(R.id.add_new)?.visibility = View.VISIBLE
//            }
//        })

        return binding?.root
    }

    private fun loadCategories(viewModel: HomeViewModel) {
        viewModel.categories().subscribe(object : Observer<ArrayList<Category>> {
            override fun onComplete() {
                Log.d(TAG, "data stream ended")
            }

            override fun onSubscribe(d: Disposable) {
                Log.d(TAG, "data stream started")
                compositeDisposable.add(d)

            }

            override fun onNext(t: ArrayList<Category>) {
                binding?.loader?.visibility = View.GONE
                if (t.isEmpty()) {
                    binding?.notFoundMsg?.visibility = View.VISIBLE
                } else {
                    val featuredList = t.filter { category -> category.isFeatured == 1 }
                    binding?.categoryRecycler?.visibility = View.VISIBLE
                    binding?.categoryRecycler?.adapter = CategoryAdapter(t, context)
                    binding?.categoryRecycler?.layoutManager = LinearLayoutManager(context)
                    binding?.categoryRecycler?.setHasFixedSize(true)
                    binding?.categoryRecycler?.isNestedScrollingEnabled = true

//                binding?.featuredCategory?.visibility = View.GONE

                    binding?.featuredCategory?.visibility = View.VISIBLE
                    binding?.recyclerviewPagerIndicatorHorizontal?.visibility = View.VISIBLE
                    binding?.recyclerviewPagerIndicatorHorizontal?.attachToRecyclerView(binding?.featuredCategory)
                    binding?.featuredCategory?.adapter = FeaturedCategoryAdapter(featuredList, context)
                    binding?.featuredCategory?.layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                }
            }

            override fun onError(e: Throwable) {
                Log.e(TAG, " an error has occurred ${e.message}")
                binding?.notFoundMsg?.visibility = View.VISIBLE
                binding?.loader?.visibility = View.GONE
                binding?.tryAgain?.visibility = View.VISIBLE

            }
        })
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}