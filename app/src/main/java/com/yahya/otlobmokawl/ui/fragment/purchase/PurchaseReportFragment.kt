package com.yahya.otlobmokawl.ui.fragment.purchase

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.PurchaseReportFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.PurchasesAdapter
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PurchaseReportFragment : Fragment() {

    @Inject
    lateinit var userRepo: IUserRepo

    var compositeDisposable = CompositeDisposable()

    var binding: PurchaseReportFragmentBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.purchase_report_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: PurchaseReportViewModel =
            ViewModelProviders.of(this, PurchaseViewModelFactory(userRepo)).get(PurchaseReportViewModel::class.java)

        compositeDisposable.add(viewModel.userData().subscribe { currentUser ->
            compositeDisposable.add(viewModel.userCredit().subscribe {
                binding?.postsCredit?.text = getString(R.string.posts_count, it.posts)
                binding?.bidCredit?.text = getString(R.string.bids_count, it.bids)
                if (currentUser?.type == Constants.USER_TYPE_CONTRACTOR) {
                    binding?.bidCreditLayout?.visibility = View.VISIBLE
                }
            })
            currentUser.apiToken?.let {
                compositeDisposable.add(viewModel.userPurchasesReport(it).subscribe {
                    if (it.status == 1) {
                        binding?.purchasesRecycler?.adapter = PurchasesAdapter(it.invoices)
                        binding?.purchasesRecycler?.layoutManager = LinearLayoutManager(context)
                    }
                })
            }
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}