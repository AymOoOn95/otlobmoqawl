package com.yahya.otlobmokawl.ui.fragment.contractor.update

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Contractor
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.ContractorRegisterFragmentBinding
import com.yahya.otlobmokawl.ui.fragment.home.HomeFragment
import com.yahya.otlobmokawl.util.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import javax.inject.Inject

class UpdateContractorFragment : Fragment() {

    @Inject
    lateinit var contractorRepo: IContractorRepo

    @Inject
    lateinit var userRepo: IUserRepo

    var binding: ContractorRegisterFragmentBinding? = null

    var imagePart: MultipartBody.Part? = null

    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.contractor_register_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        binding?.let { nBinding ->
            nBinding.contractorPassword.hint = resources.getString(R.string.new_password)
            nBinding.nationalId.visibility = View.GONE
            nBinding.contractorSid.visibility = View.GONE
            nBinding.contractorEmail.visibility = View.GONE
            nBinding.contractorOldPassword.visibility = View.VISIBLE
            nBinding.nationalIdOptional.visibility = View.GONE
            nBinding.uploadImage.visibility = View.VISIBLE
            nBinding.uploadImage.setOnClickListener {
                RxPaparazzo.single(this).usingFiles().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        if (it.resultCode() == Activity.RESULT_OK) {
                            it.data().file?.let {
                                nBinding.uploadImage.setImageURI(it.toUri())
                                Glide.with(nBinding.uploadImage).load(it).into(nBinding.uploadImage)
                                imagePart = it.toImagePart("image")

                            }
                        }

                    }
            }
            nBinding.register.text = resources.getString(R.string.update)
            val viewModel: UpdateContractorViewModel =
                ViewModelProviders.of(this, UpdateViewModelFactory(userRepo, contractorRepo))
                    .get(UpdateContractorViewModel::class.java)
            compositeDisposable.add(viewModel.userLocalData().subscribe {
                it.apiToken?.let { apiToken ->
                    compositeDisposable.add(viewModel.userOnlineData(apiToken).subscribe { onlineUser ->
                        onlineUser.image?.let {
                            val options = RequestOptions()
                                .centerCrop()
                                .placeholder(R.drawable.ic_my_account_black)
                                .error(R.drawable.ic_my_account_black)
                                .diskCacheStrategy(DiskCacheStrategy.ALL)

                            Glide.with(nBinding.uploadImage).load(it).apply(options).into(nBinding.uploadImage)
                        }
                        nBinding.contractorName.setText(onlineUser.name)
                        nBinding.contractorPhone.setText(onlineUser.phoneNumber)
                        nBinding.contractorAddress.setText(onlineUser.contractor?.address)
                        nBinding.contractorTitle.setText(onlineUser.contractor?.jobTitle)
                        nBinding.contractorDescription.setText(onlineUser.contractor?.about)
                        var areaId: Int = -1
                        compositeDisposable.add(viewModel.locations().subscribe {
                            it.add(0, Address(-1, -1, "أختر البلد"))
                            nBinding.countrySpinner.adapter =
                                ArrayAdapter<Address>(context, R.layout.support_simple_spinner_dropdown_item, it)
                            nBinding.countrySpinner.onItemSelectedListener =
                                object : AdapterView.OnItemSelectedListener {
                                    override fun onNothingSelected(parent: AdapterView<*>?) {

                                    }

                                    override fun onItemSelected(
                                        parent: AdapterView<*>?,
                                        view: View?,
                                        position: Int,
                                        id: Long
                                    ) {
                                        if (position > 0) {
                                            compositeDisposable.add(viewModel.locations((parent?.selectedItem as Address).id).subscribe {
                                                nBinding.citySpinner.adapter = ArrayAdapter<Address>(
                                                    context,
                                                    R.layout.support_simple_spinner_dropdown_item,
                                                    it
                                                )
                                                nBinding.citySpinner.onItemSelectedListener =
                                                    object : AdapterView.OnItemSelectedListener {
                                                        override fun onNothingSelected(parent: AdapterView<*>?) {

                                                        }

                                                        override fun onItemSelected(
                                                            parent: AdapterView<*>?,
                                                            view: View?,
                                                            position: Int,
                                                            id: Long
                                                        ) {
                                                            areaId = (parent?.selectedItem as Address).id ?: -1
                                                        }
                                                    }
                                            })
                                        }
                                    }
                                }
                        })

                        var newPassword: String? = null

                        nBinding.register.setOnClickListener {
                            val contractor = Contractor()
                            contractor.apiToken = apiToken
                            if (nBinding.contractorName.text.isNotEmpty()) {
                                contractor.name = nBinding.contractorName.text.toString()
                            } else {
                                context?.makeToast(resources.getString(R.string.name_required))
                                return@setOnClickListener
                            }
//                            if (nBinding.contractorTitle.text.isNotEmpty()) {
//                                contractor.jobTitle = nBinding.contractorTitle.text.toString()
//                            } else {
//                                context?.makeToast(resources.getString(R.string.job_title_required))
//                                return@setOnClickListener
//                            }
//                            if (nBinding.contractorDescription.text.isNotEmpty()) {
//                                contractor.about = nBinding.contractorDescription.text.toString()
//                            } else {
//                                context?.makeToast(resources.getString(R.string.job_description_required))
//                                return@setOnClickListener
//                            }
                            if (nBinding.contractorPhone.text.isNotEmpty()) {
                                contractor.phone = nBinding.contractorPhone.text.toString()
                            } else {
                                context?.makeToast(resources.getString(R.string.phone_required))
                                return@setOnClickListener
                            }
                            if (areaId != -1) {
                                if (nBinding.contractorAddress.text.isNotEmpty()) {
                                    contractor.address = nBinding.contractorAddress.text.toString()
                                    contractor.areaId = areaId
                                } else {
                                    context?.makeToast(resources.getString(R.string.address_required))
                                    return@setOnClickListener
                                }
                            }

                            if (nBinding.contractorPassword.text.isNotEmpty()) {
                                if (nBinding.contractorOldPassword.text.isNotEmpty()) {
                                    contractor.password = nBinding.contractorOldPassword.text.toString()
                                    newPassword = nBinding.contractorPassword.text.toString()
                                } else {
                                    context?.makeToast(resources.getString(R.string.password_required))
                                    return@setOnClickListener
                                }
                            }

                            viewModel.updateContractor(contractor, newPassword, imagePart)
                                .subscribe(object : Observer<RegisterResponse> {
                                    override fun onComplete() {

                                    }

                                    override fun onSubscribe(d: Disposable) {
                                        compositeDisposable.add(d)
                                    }

                                    override fun onNext(t: RegisterResponse) {
                                        if (t.status == 1) {
                                            contractor.image = t.user?.image
                                            contractor.type = Constants.USER_TYPE_CONTRACTOR
                                            viewModel.saveContractorData(contractor)
                                            context?.makeToast(resources.getString(R.string.update_done))
                                            fragmentManager?.transaction {
                                                replace(R.id.fragment_container, HomeFragment())
                                            }
                                            activity?.onBackPressed()

                                        } else {
                                            context?.makeToast(resources.getString(R.string.update_failed))
                                        }
                                    }

                                    override fun onError(e: Throwable) {
                                        Log.e("UpdateContractorFragme", "An Error ", e)
                                    }
                                })

                        }
                    })
                }
            })
        }
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}