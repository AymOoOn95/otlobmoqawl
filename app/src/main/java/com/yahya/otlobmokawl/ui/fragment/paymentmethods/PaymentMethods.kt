package com.yahya.otlobmokawl.ui.fragment.paymentmethods

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.PaymentMethodsFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.PaymentMethodsAdapter
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PaymentMethods : Fragment() {

    var binding: PaymentMethodsFragmentBinding? = null

    @Inject
    lateinit var userRepo: IUserRepo

    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.payment_methods_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: PaymentMethodsViewModel = ViewModelProviders.of(this, PaymentMethodsViewModelFactory(userRepo))
            .get(PaymentMethodsViewModel::class.java)
        compositeDisposable.add(viewModel.paymentMetods().subscribe { paymentMethods ->
            context?.let {
                binding?.paymentMethods?.setAdapter(PaymentMethodsAdapter(paymentMethods, it))
            }
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }

}