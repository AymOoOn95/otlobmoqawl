package com.yahya.otlobmokawl.ui.fragment.memberships

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Membership
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.MembershipsFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.MembershipsAdapter
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class MembershipsFragment : Fragment() {

    @Inject
    lateinit var repo: IUserRepo

    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity?.application as MyApplication).appComponent?.inject(this)
        val binding: MembershipsFragmentBinding =
            DataBindingUtil.inflate(inflater, R.layout.memberships_fragment, container, false)

        val viewModel: MembershipsViewModel =
            ViewModelProviders.of(this, MembershipsViewModelFactory(repo)).get(MembershipsViewModel::class.java)
        viewModel.getMemberships().subscribe(object : Observer<ArrayList<Membership>> {
            override fun onComplete() {

            }

            override fun onSubscribe(d: Disposable) {
                compositeDisposable.add(d)
            }

            override fun onNext(t: ArrayList<Membership>) {
                binding.memberships.adapter = MembershipsAdapter(t, context)
                binding.memberships.layoutManager = LinearLayoutManager(context)
            }

            override fun onError(e: Throwable) {

            }
        })
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}