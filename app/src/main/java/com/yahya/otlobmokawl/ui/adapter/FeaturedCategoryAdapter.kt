package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.cardview.widget.CardView
import androidx.core.os.bundleOf
import androidx.fragment.app.transaction
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Category
import com.yahya.otlobmokawl.ui.fragment.posts.allposts.AllPostsFragment
import com.yahya.otlobmokawl.util.Constants

class FeaturedCategoryAdapter(val categories: List<Category>,val context:Context?) :
    RecyclerView.Adapter<FeaturedCategoryAdapter.FeaturedCategoryViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FeaturedCategoryViewHolder {
        return FeaturedCategoryViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.faetured_category_item_layout,
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: FeaturedCategoryViewHolder, position: Int) {
        holder.categoryName.text = categories[position].name
        holder.categoryItem.setOnClickListener {
            context?.let {
                val fragment = AllPostsFragment()
                fragment.arguments = bundleOf(Pair(Constants.CATEGORY_ID_ARGUMENT_KEY,categories[position].id))
                (it as AppCompatActivity).supportFragmentManager.transaction {
                    replace(R.id.fragment_container,fragment)
                }
            }
        }
    }

    override fun getItemCount(): Int = categories.size
    class FeaturedCategoryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var categoryName: TextView
        var categoryItem: CardView

        init {
            categoryName = itemView.findViewById(R.id.category_name)
            categoryItem = itemView.findViewById(R.id.category_item)
        }
    }
}