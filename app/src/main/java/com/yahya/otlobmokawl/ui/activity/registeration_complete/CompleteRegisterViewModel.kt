package com.yahya.otlobmokawl.ui.activity.registeration_complete

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CompleteRegisterViewModel(val userRepo: IUserRepo) : ViewModel() {
    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getLang(): String {
        return userRepo.getLanguage()
    }
}