package com.yahya.otlobmokawl.data.repo

import android.content.SharedPreferences
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.network.AppApi
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.util.Constants
import io.reactivex.Observable
import okhttp3.MultipartBody

class ClientRepo(val appApi: AppApi, val loginShared: SharedPreferences) : IClientRepo {
    override fun registerClient(client: Client, images: ArrayList<MultipartBody.Part?>): Observable<RegisterResponse> {
        var response: Observable<RegisterResponse> = appApi.register(
            null,
            client.email,
            client.phone,
            client.name,
            client.password,
            Constants.USER_TYPE_CLIENT,
            client.sid,
            client.areaId,
            client.address,
            null, null, images
        )
        client.apiToken?.let {
            response = appApi.completeRegister(
                client.apiToken,
                null,
                client.phone,
                client.name,
                client.password,
                client.userType,
                client.sid,
                client.areaId,
                client.address,
                null, null, images
            )
        }
        return response
    }

    override fun answerQuestion(question: Question, apiToken: String): Observable<Question> {
        return this.appApi.answerQuestion(question.id, apiToken, question.answer)
    }

    override fun acceptBid(bid: Bid) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun finishProject(rate: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun updateClientData(
        client: Client,
        newPassword: String?,
        imagePart: MultipartBody.Part?
    ): Observable<RegisterResponse> {
        return appApi.updateAccount(
            client.apiToken,
            client.password,
            newPassword,
            client.name,
            client.userAreaId,
            client.address,
            client.phone,
            null,
            null,
            imagePart
        )
    }

    override fun questions4Me(apiToken: String): Observable<ArrayList<Question>> {
        return appApi.questions4Me(apiToken)
    }
}