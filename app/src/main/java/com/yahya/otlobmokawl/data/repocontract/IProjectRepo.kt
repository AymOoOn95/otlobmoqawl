package com.yahya.otlobmokawl.data.repocontract

import com.yahya.otlobmokawl.data.entity.*
import com.yahya.otlobmokawl.data.network.response.AddPostResponse
import com.yahya.otlobmokawl.data.network.response.MyListResponse
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import io.reactivex.Observable
import okhttp3.MultipartBody

interface IProjectRepo {

    fun addProject(
        project: Post,
        apiToken: String,
        isContractor: String,
        images: ArrayList<MultipartBody.Part?>?
    ): Observable<AddPostResponse>

    fun updateProject(project: Post)
    fun deleteProject(project: Post)
    fun getProjects(apiToken: String, categoryId: Int): Observable<ArrayList<Post>>
    fun getProjectQuestions(projectId: Int): Observable<List<Question>>
    fun getProjectBids(projectId: Int, apiToken: String): Observable<ArrayList<Bid>>
    fun getCategories(categoryId: Int?, lang: String): Observable<ArrayList<Category>>
    fun addToFavourite(postId: Int, apiToken: String): Observable<RegisterResponse>
    fun getFavourites(apiToken: String): Observable<MyListResponse>
    fun selectBid(bidId: Int, apiToken: String): Observable<Bid>
}