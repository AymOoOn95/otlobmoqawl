package com.yahya.otlobmokawl.di

import android.content.Context
import com.yahya.otlobmokawl.data.network.AppApi
import com.yahya.otlobmokawl.util.Constants
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

@Module
class NetworkModule(val context: Context) {

    @Provides
    fun provideChuckInterceptor(): ChuckInterceptor {
        return ChuckInterceptor(context).showNotification(true)
    }

    @Provides
    fun provideOkHttpClient(chuckInterceptor: ChuckInterceptor): OkHttpClient {
        return OkHttpClient.Builder()
            .addInterceptor(chuckInterceptor)
            .connectTimeout(15, TimeUnit.MINUTES)
            .readTimeout(15, TimeUnit.MINUTES)
            .writeTimeout(15, TimeUnit.MINUTES).build()
    }

    @Provides
    fun provideAppApi(okHttpClient: OkHttpClient): AppApi {
        return Retrofit.Builder()
            .baseUrl(Constants.API_URL)
            .client(okHttpClient)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(AppApi::class.java)
    }
}