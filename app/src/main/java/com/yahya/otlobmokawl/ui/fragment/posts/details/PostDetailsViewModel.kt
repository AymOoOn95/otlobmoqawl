package com.yahya.otlobmokawl.ui.fragment.posts.details

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.AddBidResponse
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repo.ProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class PostDetailsViewModel(
    val projectRepo: IProjectRepo,
    val contractorRepo: IContractorRepo,
    val userRepo: IUserRepo
) : ViewModel() {

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun askQuestion(question: Question, apiToken: String, postId: Int): Observable<RegisterResponse> {
        return contractorRepo.askQuestion(question, apiToken, postId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }


    fun postQuestion(postId: Int, apiToken: String): Observable<ArrayList<Question>> {
        return (projectRepo as ProjectRepo).postQuestions(postId, apiToken, userRepo.getLanguage())
            .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
// map {
//                val result = ArrayList<Question>()
//                for (item: Question in it) {
//                    item.answer?.let {
//                        result.add(item)
//                    }
//                }
//                return@map result
//            }.observeOn(AndroidSchedulers.mainThread())

        /*
        * .map {
                for (question: Question in it) {
                    if (question.answer == null) {
                        it.remove(question)
                    }
                }
                return@map it
            }
        * */
    }

    fun sendBid(apiToken: String, bid: Bid): Observable<AddBidResponse> {
        return contractorRepo.makeBid(bid, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun myCredits(): Observable<UserCredit> {
        return userRepo.myCreditsFromShared().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveUserCredit(userCredit: UserCredit) {
        userRepo.saveCredit(userCredit)
    }

    fun addPost2Fav(postId: Int, apiToken: String): Observable<RegisterResponse> {
        return projectRepo.addToFavourite(postId, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}