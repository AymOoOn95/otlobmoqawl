package com.yahya.otlobmokawl.ui.activity.registeration_complete

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.ActivityCompleteRegisterBinding
import com.yahya.otlobmokawl.util.LanguageChange
import javax.inject.Inject

class CompleteRegisterActivity : AppCompatActivity() {

    var binding: ActivityCompleteRegisterBinding? = null

    @Inject
    lateinit var userRepo: IUserRepo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_complete_register)
        val viewModel: CompleteRegisterViewModel =
            ViewModelProviders.of(this, CompleteReigsterViewModelFactory(userRepo))
                .get(CompleteRegisterViewModel::class.java)
        LanguageChange.changeTo(viewModel.getLang(), this)

    }
}
