package com.yahya.otlobmokawl.ui.fragment.posts.details

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class PostDetailsViewModelFactory(
    val projectRepo: IProjectRepo,
    val contractorRepo: IContractorRepo,
    val userRepo: IUserRepo
) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostDetailsViewModel(projectRepo, contractorRepo, userRepo) as T
    }
}