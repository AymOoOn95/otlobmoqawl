package com.yahya.otlobmokawl.util

import android.app.Activity
import android.content.res.Configuration
import java.util.*

class LanguageChange {
    companion object {
        fun changeTo(language: String, activity: Activity) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            activity.baseContext.resources.updateConfiguration(
                config,
                activity.baseContext.resources.displayMetrics
            )
        }
    }
}