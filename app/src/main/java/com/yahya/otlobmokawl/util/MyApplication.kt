package com.yahya.otlobmokawl.util

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.yahya.otlobmokawl.di.*
import com.facebook.FacebookSdk
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo

class MyApplication : MultiDexApplication() {

    var appComponent: AppComponent? = null
    override fun onCreate() {
        super.onCreate()
        RxPaparazzo.register(this)

        appComponent =
            DaggerAppComponent.builder()
                .localStorageModule(LocalStorageModule(this))
                .repoModule(RepoModule(this))
                .networkModule(NetworkModule(baseContext))
                .build()
    }
}