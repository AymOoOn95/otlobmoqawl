package com.yahya.otlobmokawl.ui.fragment.client.update

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

class ClientUpdateViewModel(private val userRepo: IUserRepo, private val clientRepo: IClientRepo) : ViewModel() {

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun locations(parentId: Int? = null): Observable<ArrayList<Address>> {
        return (userRepo as UserRepo).getLocations(parentId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun updateClientData(
        client: Client,
        newPassword: String?,
        userImagePart: MultipartBody.Part?
    ): Observable<RegisterResponse> {
        return clientRepo.updateClientData(client, newPassword, userImagePart).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun saveClientData(client: Client) {
        userRepo.saveUserData(client as User)
    }
}