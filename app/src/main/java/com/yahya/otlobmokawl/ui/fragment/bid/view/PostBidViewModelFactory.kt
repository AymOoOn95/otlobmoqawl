package com.yahya.otlobmokawl.ui.fragment.bid.view

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo

class PostBidViewModelFactory(val projectRepo: IProjectRepo) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return PostBidViewModel(projectRepo) as T
    }
}