package com.yahya.otlobmokawl.ui.fragment.posts.allposts

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class AllPostsViewModelFactory(val projectRepo: IProjectRepo, val userRepo: IUserRepo) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AllPostsViewModel(projectRepo, userRepo) as T
    }
}