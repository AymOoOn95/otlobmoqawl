package com.yahya.otlobmokawl.ui.activity.credit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.billingclient.api.*
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.MyCreditFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.PurchaseItemsAdapter
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.LanguageChange
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MyCreditActivity : AppCompatActivity(), PurchasesUpdatedListener, PurchaseItemsAdapter.OnBuyItemClickListener {
    val TAG = "MyCreditActivity"

    var binding: MyCreditFragmentBinding? = null
    var billingClient: BillingClient? = null
    @Inject
    lateinit var userRepo: IUserRepo

    var viewModel: MyCreditViewModel? = null
    var currentUser: User? = null

    var clickedItem: SkuDetails? = null

    val compositeDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.my_credit_fragment)
        (application as MyApplication).appComponent?.inject(this)
        viewModel =
            ViewModelProviders.of(this, MyCreditViewModelFactory(userRepo)).get(MyCreditViewModel::class.java)
        LanguageChange.changeTo(viewModel?.getLang() ?: Constants.LANG_EN, this)

        compositeDisposable.add(viewModel!!.userData().subscribe { user ->
            user?.apiToken?.let {
                currentUser = user
            }

            compositeDisposable.add(viewModel!!.userCreditFromShared().subscribe {
                binding?.postsCredit?.text = resources.getString(R.string.posts_count, it.posts)
                if (currentUser?.type == Constants.USER_TYPE_CONTRACTOR) {
                    binding?.bidCredit?.text = resources.getString(R.string.bids_count, it.bids)
                    binding?.bidCreditLayout?.visibility = View.VISIBLE
                }
            })

            billingClient = BillingClient.newBuilder(this).setListener(this).build()
            billingClient?.startConnection(object : BillingClientStateListener {
                override fun onBillingServiceDisconnected() {
                    Log.d(TAG, "Google Play service is disconnected")
                }

                override fun onBillingSetupFinished(responseCode: Int) {
                    if (responseCode == BillingClient.BillingResponse.OK) {
                        Log.d(TAG, "Google Play service is connected successfully")
                        var skuNames: List<String> = listOf(
                            Constants.CLIENT_VIP_SKU,
                            Constants.CLIENT_BASIC_SKU,
                            Constants.CLIENT_MEDIUM_SKU,
                            Constants.CLIENT_EXTREM_SKU
                        )
                        if (currentUser?.type == Constants.USER_TYPE_CONTRACTOR) {
                            skuNames = skuNames.plus(
                                listOf(
                                    Constants.CONTRACTOR_VIP_SKU,
                                    Constants.CONTRACTOR_BASIC_SKU,
                                    Constants.CONTRACTOR_MEDIUM_SKU,
                                    Constants.CONTRACTOR_EXTREM_SKU
                                )
                            )
                        }
                        val skuDetailsParams =
                            SkuDetailsParams.newBuilder().setSkusList(
                                skuNames
                            ).setType(BillingClient.SkuType.INAPP).build()

                        billingClient?.querySkuDetailsAsync(
                            skuDetailsParams
                        ) { responseCode, skuDetailsList ->
                            binding?.purchaseItemRecycler?.adapter =

                                PurchaseItemsAdapter(skuDetailsList, this@MyCreditActivity, this@MyCreditActivity)
                            binding?.purchaseItemRecycler?.layoutManager = LinearLayoutManager(this@MyCreditActivity)

                        }
                    } else {
                        Log.d(TAG, "Google Play service Error with code $responseCode")
                    }
                }
            })

        })

    }

    override fun onPurchasesUpdated(responseCode: Int, purchases: MutableList<Purchase>?) {
        Log.d(TAG, "onPurchasesUpdated() response: $responseCode")
        if (responseCode == BillingClient.BillingResponse.OK) {
            billingClient?.consumeAsync(purchases!![0].purchaseToken, object : ConsumeResponseListener {
                override fun onConsumeResponse(responseCode: Int, purchaseToken: String?) {

                }
            })
            currentUser?.let { user ->
                clickedItem?.let {
                    Log.d(TAG, "${purchases!![0].orderId.length}")
                    Log.d(TAG, "${purchases!![0].purchaseToken.length}")
                    Log.d(TAG, "${purchases!![0].signature.length}")
                    compositeDisposable.add(
                        viewModel!!.makePurchase(
                            user.apiToken!!,
                            it.sku,
                            purchases[0].orderId,
                            purchases[0].purchaseToken,
                            purchases[0].signature,
                            purchases[0].purchaseTime.toString(),
                            it.price,
                            it.priceCurrencyCode
                        ).subscribe { buyCreditResponse ->
                            if (buyCreditResponse.status == 1) {
                                compositeDisposable.add(viewModel!!.userCreditFromShared().subscribe {
                                    it.posts = it.posts.plus(buyCreditResponse.credit.createPost)
                                    it.bids = it.bids.plus(buyCreditResponse.credit.createBids)
                                    viewModel?.saveUserCredit2Shared(it)
                                    binding?.postsCredit?.text = resources.getString(R.string.posts_count, it.posts)
                                    binding?.bidCredit?.text = resources.getString(R.string.bids_count, it.bids)

                                })
                                this.makeToast(getString(R.string.purchase_done))
                            }

                        }
                    )
                }
            }
        }

    }

    override fun onBuyClicked(item: SkuDetails) {
        clickedItem = item
        val billingFlowParams = BillingFlowParams.newBuilder().setSkuDetails(item).build()
        billingClient?.launchBillingFlow(this, billingFlowParams)

    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
    }
}
