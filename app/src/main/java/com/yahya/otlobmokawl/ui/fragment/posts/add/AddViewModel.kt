package com.yahya.otlobmokawl.ui.fragment.posts.add

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.*
import com.yahya.otlobmokawl.data.network.response.AddPostResponse
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

class AddViewModel(val projectRepo: IProjectRepo, val userRepo: IUserRepo) : ViewModel() {

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun categories(parentId: Int? = null): Observable<ArrayList<Category>> {
        return projectRepo.getCategories(parentId, userRepo.getLanguage()).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun locations(parentId: Int?): Observable<ArrayList<Address>> {
        return (userRepo as UserRepo).getLocations(parentId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun addPost(
        post: Post,
        apiToken: String,
        isContractor: String,
        images: ArrayList<MultipartBody.Part?>?
    ): Observable<AddPostResponse> {
        return projectRepo.addProject(post, apiToken, isContractor, images).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun updateUserCredit(availableCredit: UserCredit) {
        userRepo.saveCredit(availableCredit)
    }
}