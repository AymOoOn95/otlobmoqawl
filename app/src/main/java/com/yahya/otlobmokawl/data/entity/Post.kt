package com.yahya.otlobmokawl.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Post(
    var id: Int?,
    @SerializedName("user_id")
    var userId: Int?,
    var title: String?,
    @SerializedName("category_id")
    var categoryId: Int?,
    @SerializedName("sub_categories")
    var subCategory: Int?,
    @SerializedName("area_id")
    var areaId: Int?,
    var address: String?,
    var content: String?,
    @SerializedName("main_image")
    var mainImage: String?,
    @SerializedName("bid_id")
    var bidId: Int?,
    var status: Int?,
    @SerializedName("is_mine")
    var isMine: Int?,
    @SerializedName("sub_category")
    var subCategoryStr: String?,
    @SerializedName("is_ad")
    var isAdd: Int?,
    var client: Client?,
    var contractor: Contractor?,
    var bid: Bid?,
    var icon: String?,
    var images: List<Image>?,
    var image: String?,
    var date: String?,
    @SerializedName("is_favorite")
    var isInFavourite: Int?,
    @SerializedName("is_bided")
    var isBided: Int?,
    var bids: List<Bid>?,
    var questions: List<Question>?
) : Parcelable {
}

/*
*
*
        "": 0,
        "": 0,
        "is_reported": 0,
        "image": null,
        "": "ادوات ومعدات بناء",
        "images": [],
        "contractor": null

        */