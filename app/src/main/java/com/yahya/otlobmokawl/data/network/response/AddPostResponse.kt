package com.yahya.otlobmokawl.data.network.response

import com.google.gson.annotations.SerializedName
import com.yahya.otlobmokawl.data.entity.Post

data class AddPostResponse(
    val post: Post,
    val status: Int,
    @SerializedName("available_posts")
    val availablePosts: Int
)