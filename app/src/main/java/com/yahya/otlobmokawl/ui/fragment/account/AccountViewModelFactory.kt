package com.yahya.otlobmokawl.ui.fragment.account

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class AccountViewModelFactory(val userRepo:IUserRepo):ViewModelProvider.Factory{

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return AccountViewModel(userRepo) as T
    }
}