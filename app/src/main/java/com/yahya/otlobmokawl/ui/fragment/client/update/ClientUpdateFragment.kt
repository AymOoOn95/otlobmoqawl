package com.yahya.otlobmokawl.ui.fragment.client.update

import android.app.Activity
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.core.net.toUri
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.miguelbcr.ui.rx_paparazzo2.RxPaparazzo
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.UserRegisterFragmentBinding
import com.yahya.otlobmokawl.ui.fragment.home.HomeFragment
import com.yahya.otlobmokawl.util.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import javax.inject.Inject


class ClientUpdateFragment : Fragment() {

    @Inject
    lateinit var userRepo: IUserRepo

    @Inject
    lateinit var clientRepo: IClientRepo

    var binding: UserRegisterFragmentBinding? = null
    val compositeDisposable = CompositeDisposable()
    var userImagePart: MultipartBody.Part? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity?.application as MyApplication).appComponent?.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.user_register_fragment, container, false)
        val viewModel: ClientUpdateViewModel =
            ViewModelProviders.of(this, ClientUpdateViewModelFactory(userRepo, clientRepo))
                .get(ClientUpdateViewModel::class.java)
        binding?.let { nBinding ->
            nBinding.userType.visibility = View.GONE
            nBinding.clientPassword.hint = resources.getString(R.string.new_password)
            nBinding.clientEmail.visibility = View.GONE
            nBinding.idCard.visibility = View.GONE
            nBinding.clientOldPassword.visibility = View.VISIBLE
            nBinding.registerClient.text = context?.resources?.getString(R.string.update)
            nBinding.nationalIdOptional.visibility = View.GONE
            nBinding.uploadImage.visibility = View.VISIBLE
            nBinding.uploadImage.setOnClickListener {
                RxPaparazzo.single(this).usingFiles().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe {
                        if (it.resultCode() == Activity.RESULT_OK) {
                            it.data().file?.let {
                                nBinding.uploadImage.setImageURI(it.toUri())
                                Glide.with(nBinding.uploadImage).load(it).into(nBinding.uploadImage)
                                userImagePart = it.toImagePart("image")

                            }
                        }

                    }

            }
            compositeDisposable.add(viewModel.userData().subscribe { currentUser ->
                nBinding.clientName.setText(currentUser.name)
                nBinding.clientPhone.setText(currentUser.phone)
                nBinding.clientAddress.setText(currentUser.address)
                nBinding.registerClient.text = resources.getString(R.string.update)
                currentUser.image?.let {
                    val options = RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.ic_my_account_black)
                        .error(R.drawable.ic_my_account_black)
                        .diskCacheStrategy(DiskCacheStrategy.ALL)

                    Glide.with(nBinding.uploadImage).load(it).apply(options).into(nBinding.uploadImage)
                }
                var areaId: Int? = null
                compositeDisposable.add(viewModel.locations().subscribe {
                    it.add(0, Address(-1, -1, "اختر البلد"))
                    nBinding.countrySpinner.adapter =
                        ArrayAdapter<Address>(context, R.layout.support_simple_spinner_dropdown_item, it)

                    nBinding.countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {

                        }

                        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                            if (position > 0) {
                                compositeDisposable.add(viewModel.locations((parent?.selectedItem as Address).id).subscribe {
                                    nBinding.citySpinner.adapter = ArrayAdapter<Address>(
                                        context,
                                        R.layout.support_simple_spinner_dropdown_item,
                                        it
                                    )

                                    nBinding.citySpinner.onItemSelectedListener =
                                        object : AdapterView.OnItemSelectedListener {
                                            override fun onNothingSelected(parent: AdapterView<*>?) {

                                            }

                                            override fun onItemSelected(
                                                parent: AdapterView<*>?,
                                                view: View?,
                                                position: Int,
                                                id: Long
                                            ) {
                                                areaId = (parent?.selectedItem as Address).id
                                            }
                                        }
                                })
                            }
                        }
                    }
                })


                nBinding.registerClient.setOnClickListener {
                    val user = Client()
                    user.apiToken = currentUser.apiToken
                    var newPassword: String? = null
                    if (nBinding.clientName.text.isNotEmpty()) {
                        user.name = nBinding.clientName.text.toString()
                    } else {
                        context?.makeToast(getString(R.string.name_required))
                        return@setOnClickListener
                    }

                    if (nBinding.clientPhone.text.toString().isNotEmpty()) {
                        user.phone = nBinding.clientPhone.text.toString()
                    } else {
                        context?.makeToast(getString(R.string.phone_required))
                        return@setOnClickListener
                    }

                    if (nBinding.clientPassword.text.isNotEmpty()) {
                        if (nBinding.clientOldPassword.text.isNotEmpty()) {
                            user.password = nBinding.clientOldPassword.text.toString()
                            newPassword = nBinding.clientPassword.text.toString()
                        }
                    }
                    areaId?.let {
                        if (nBinding.clientAddress.text.isNotEmpty()) {
                            user.areaId = it
                            user.address = nBinding.clientAddress.text.toString()
                        } else {
                            context?.makeToast(getString(R.string.address_required))
                            return@setOnClickListener
                        }
                    }

                    viewModel.updateClientData(user, newPassword, userImagePart)
                        .subscribe(object : Observer<RegisterResponse> {
                            override fun onComplete() {

                            }

                            override fun onSubscribe(d: Disposable) {
                                compositeDisposable.add(d)
                            }

                            override fun onNext(t: RegisterResponse) {
                                if (t.status == 1) {
                                    context?.makeToast(resources.getString(R.string.update_done))
                                    user.image = t.user?.image
                                    user.type = Constants.USER_TYPE_CLIENT
                                    viewModel.saveClientData(user)
                                    fragmentManager?.transaction {
                                        replace(R.id.fragment_container, HomeFragment())
                                    }
                                    activity?.onBackPressed()
                                } else {
                                    context?.makeToast(resources.getString(R.string.update_failed))
                                }
                            }

                            override fun onError(e: Throwable) {
                                Log.e("UpDateClient", "An Error ", e)
                            }
                        })
                }
            })
        }
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}