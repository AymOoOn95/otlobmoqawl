package com.yahya.otlobmokawl.data.entity

class PaymentMethods(
    var id: Int?,
    var name: String?,
    var image: String?,
    var details: ArrayList<PaymentMethodDetails>?
) {

    override fun toString(): String {
        return this.name ?: "Object has no name"
    }
}