package com.yahya.otlobmokawl.ui.fragment.bid

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.databinding.AddsBidsDetailsBinding

class BidFragment : Fragment() {
    private var binding: AddsBidsDetailsBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.adds_bids_details, container, false)
        return binding?.root
    }
}