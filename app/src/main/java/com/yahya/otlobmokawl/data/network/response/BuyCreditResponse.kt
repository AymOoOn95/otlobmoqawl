package com.yahya.otlobmokawl.data.network.response

import com.google.gson.annotations.SerializedName

class BuyCreditResponse(
    var status: Int,
    var credit: CreditResponse
)

class CreditResponse(
    var id: Int,
    @SerializedName("user_id")
    var userId: Int,
    @SerializedName("create_post")
    var createPost: Int,
    @SerializedName("create_bids")
    var createBids: Int,
    var date: String

)