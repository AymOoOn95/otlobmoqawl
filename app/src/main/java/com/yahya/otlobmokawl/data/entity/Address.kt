package com.yahya.otlobmokawl.data.entity

class Address(
    var id:Int?,
    var parentId: Int?,
    var name:String
) {

    override fun toString(): String {
        return this.name
    }
}