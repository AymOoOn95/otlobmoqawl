package com.yahya.otlobmokawl.ui.activity.credit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class MyCreditViewModelFactory(val userRepo:IUserRepo):ViewModelProvider.Factory{
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return MyCreditViewModel(userRepo) as T

    }
}