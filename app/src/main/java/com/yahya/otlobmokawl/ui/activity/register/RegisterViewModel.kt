package com.yahya.otlobmokawl.ui.activity.register

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class RegisterViewModel(val userRepo: IUserRepo) : ViewModel() {
    fun getLocations(parentId: Int?): Observable<ArrayList<Address>> {
        return (userRepo as UserRepo).getLocations(parentId)
    }

    fun userRegistration(user: User): Observable<RegisterResponse> {
        return userRepo.register(user, "")
    }

    fun saveClientInShared(user: User) {
        userRepo.saveUserData(user)
    }

    fun userCreditOnline(apiToken: String): Observable<UserCredit> {
        return userRepo.myCredits(apiToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveCredits(userCredit: UserCredit) {
        userRepo.saveCredit(userCredit)
    }

}