package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.util.Constants

class QuestionsAdapter(
    val questions: ArrayList<Question>,
    val context: Context?,
    val userType: String,
    val listener: OnSendAnswerClickListener
) :
    RecyclerView.Adapter<QuestionsAdapter.QuestionsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionsViewHolder {
        return QuestionsViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.question_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = questions.size

    override fun onBindViewHolder(holder: QuestionsViewHolder, position: Int) {
        holder.postTitle.text = questions[position].title
        holder.question.text = questions[position].question
        if (userType.equals(Constants.USER_TYPE_CONTRACTOR)) {
            holder.answerLayout.visibility = View.GONE
        }
        questions[position].answer?.let {
            holder.alreadyAnswer.visibility = View.VISIBLE
            holder.alreadyAnswer.text = it
            holder.answerLayout.visibility = View.GONE
        }
        holder.sendAnswer.setOnClickListener {
            questions[position].answer = holder.answer.text.toString()
            listener.onClick(questions[position])
        }
    }

    fun updateItem(question: Question, index: Int = -1) {
        if (index > -1) {
            this.questions.add(index, question)
        } else {
            for (item: Question in this.questions) {
                if (question.id == item.id) {
                    item.answer = question.answer
                    break
                }
            }
        }

        notifyDataSetChanged()
    }


    interface OnSendAnswerClickListener {
        fun onClick(question: Question)
    }

    class QuestionsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val postTitle: TextView
        val question: TextView
        val answer: EditText
        val sendAnswer: ImageView
        val alreadyAnswer: TextView
        val answerLayout: LinearLayout

        init {
            postTitle = itemView.findViewById(R.id.post_title)
            question = itemView.findViewById(R.id.question)
            answer = itemView.findViewById(R.id.write_answer)
            sendAnswer = itemView.findViewById(R.id.send_answer)
            alreadyAnswer = itemView.findViewById(R.id.answer_already)
            answerLayout = itemView.findViewById(R.id.send_answer_layout)
        }
    }
}