package com.yahya.otlobmokawl.data.repo

import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Category
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.entity.Question
import com.yahya.otlobmokawl.data.network.AppApi
import com.yahya.otlobmokawl.data.network.response.AddPostResponse
import com.yahya.otlobmokawl.data.network.response.MyListResponse
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import io.reactivex.Observable
import okhttp3.MultipartBody

class ProjectRepo(val appApi: AppApi) : IProjectRepo {

    override fun addToFavourite(postId: Int, apiToken: String): Observable<RegisterResponse> {
        return appApi.addPost2Fav(postId, apiToken)
    }


    override fun addProject(
        post: Post,
        apiToken: String,
        isContractor: String,
        images: ArrayList<MultipartBody.Part?>?
    ): Observable<AddPostResponse> {
        return appApi.addPost(
            apiToken,
            post.categoryId!!,
            post.subCategory!!,
            post.areaId!!,
            post.title!!,
            post.content!!,
            post.address!!,
            isContractor,
            images
        )
    }

    override fun updateProject(project: Post) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun deleteProject(project: Post) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getProjects(apiToken: String, id: Int): Observable<ArrayList<Post>> {
        return appApi.categoryPosts(id, apiToken)
    }

    override fun getProjectQuestions(projectId: Int): Observable<List<Question>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun getProjectBids(projectId: Int, apiToken: String): Observable<ArrayList<Bid>> {
        return appApi.postBids(projectId, apiToken)
    }

    override fun getCategories(categoryId: Int?, lang: String): Observable<ArrayList<Category>> {
        return appApi.categories(categoryId, lang)
    }

    fun getUserProjects(apiToken: String): Observable<MyListResponse> {
        return appApi.myList(apiToken)
    }

    override fun getFavourites(apiToken: String): Observable<MyListResponse> {
        return appApi.favourites(apiToken)
    }

    fun postQuestions(postId: Int, apiToken: String, lang: String?): Observable<ArrayList<Question>> {
        return appApi.postQuestions(postId, apiToken, lang)
    }

    override fun selectBid(bidId: Int, apiToken: String): Observable<Bid> {
        return appApi.selectBid(bidId, apiToken)
    }
}