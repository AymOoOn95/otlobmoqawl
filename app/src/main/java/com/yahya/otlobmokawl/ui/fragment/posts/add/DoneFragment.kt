package com.yahya.otlobmokawl.ui.fragment.posts.add

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.ui.fragment.home.HomeFragment

class DoneFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        var view: View = inflater.inflate(R.layout.activity_done_adding, container, false)
        view.findViewById<TextView>(R.id.go_2_main).setOnClickListener {
            activity?.supportFragmentManager?.transaction {
                replace(R.id.fragment_container, HomeFragment())
            }
        }
        return view
    }
}