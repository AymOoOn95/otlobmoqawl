package com.yahya.otlobmokawl.ui.fragment.account

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.AccountFragmentBinding
import com.yahya.otlobmokawl.ui.activity.credit.MyCreditActivity
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.ui.activity.nlogin.NLoginActivity
import com.yahya.otlobmokawl.ui.activity.register.RegisterActivity
import com.yahya.otlobmokawl.ui.fragment.client.update.ClientUpdateFragment
import com.yahya.otlobmokawl.ui.fragment.contractor.update.UpdateContractorFragment
import com.yahya.otlobmokawl.ui.fragment.informative.InformativeFragment
import com.yahya.otlobmokawl.ui.fragment.memberships.MembershipsFragment
import com.yahya.otlobmokawl.ui.fragment.paymentmethods.PaymentMethods
import com.yahya.otlobmokawl.ui.fragment.purchase.PurchaseReportFragment
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject


class AccountFragment : Fragment() {

    var binding: AccountFragmentBinding? = null

    @Inject
    lateinit var userRepo: IUserRepo

    val compositeDisposable = CompositeDisposable()
    var userType: String? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.account_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: AccountViewModel =
            ViewModelProviders.of(this, AccountViewModelFactory(userRepo)).get(AccountViewModel::class.java)
        activity?.findViewById<Button>(R.id.add_new)?.visibility = View.GONE


        compositeDisposable.add(viewModel.userData().subscribe {
            userType = it.type
        })


        viewModel.getLang()?.let {
            if (it.equals(Constants.LANG_AR)) {
                binding?.langSwitch?.isChecked = true
            }
        }
        binding?.langSwitch?.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                viewModel.changeLang(Constants.LANG_AR)
            } else {
                viewModel.changeLang(Constants.LANG_EN)
            }
            startActivity(Intent(context, MainActivity::class.java))
            activity?.finish()
        }
        binding?.logout?.setOnClickListener {
            viewModel.logout()
            activity?.supportFragmentManager?.transaction {
                startActivity(Intent(context, NLoginActivity::class.java))
                activity?.finish()
            }

        }

        binding?.about?.setOnClickListener {
            val informativeFragment = InformativeFragment()
            informativeFragment.arguments = bundleOf(Pair(Constants.INFORMATIVE_ARGS_KEY, Constants.ABOUT_ARGS_VALUE))
            activity?.supportFragmentManager?.transaction {
                replace(R.id.fragment_container, informativeFragment)
            }
        }
        binding?.memberships?.setOnClickListener {
            val membershipsFragment = MembershipsFragment()
            activity?.supportFragmentManager?.transaction {
                replace(R.id.fragment_container, membershipsFragment)
            }
        }


        binding?.privacy?.setOnClickListener {
            val informativeFragment = InformativeFragment()
            informativeFragment.arguments = bundleOf(Pair(Constants.INFORMATIVE_ARGS_KEY, Constants.PRIVACY_ARGS_VALUE))
            activity?.supportFragmentManager?.transaction {
                replace(R.id.fragment_container, informativeFragment)
            }
        }


        binding?.updateData?.setOnClickListener {
            userType?.let {
                var fragment: Fragment? = null
                if (it.equals(Constants.USER_TYPE_CLIENT)) {
                    fragment = ClientUpdateFragment()
                } else {
                    fragment = UpdateContractorFragment()
                }

                activity?.supportFragmentManager?.transaction {
                    replace(R.id.fragment_container, fragment)
                }
            }
        }

        binding?.purchaseReport?.setOnClickListener {
            activity?.supportFragmentManager?.transaction {
                replace(R.id.fragment_container, PurchaseReportFragment())
            }
        }
        binding?.myCredit?.setOnClickListener {
            userType?.let {
                if (it.equals(Constants.USER_TYPE_CLIENT) || it.equals(Constants.USER_TYPE_CONTRACTOR)) {
                    startActivity(Intent(context, MyCreditActivity::class.java))
                } else {
                    startActivity(Intent(context, RegisterActivity::class.java))
                }
            }
        }
        binding?.go2PaymentMethods?.setOnClickListener {
            activity?.supportFragmentManager?.transaction {
                replace(R.id.fragment_container, PaymentMethods())
            }
        }
        return binding?.root
    }

    override fun onDestroyView() {
        userType?.let {
            activity?.findViewById<Button>(R.id.add_new)?.visibility = View.VISIBLE
        }
        compositeDisposable.dispose()
        super.onDestroyView()

    }


}