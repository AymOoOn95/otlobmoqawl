package com.yahya.otlobmokawl.ui.fragment.bid.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.databinding.PostBidFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.MyAddsAdapter
import com.yahya.otlobmokawl.ui.adapter.MyAddsBidsAdapter
import com.yahya.otlobmokawl.ui.fragment.contractor.details.ContractorDetailsFragment
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.MyApplication
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class PostBidFragment : Fragment() {


    var binding: PostBidFragmentBinding? = null

    @Inject
    lateinit var projectRepo: IProjectRepo

    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.post_bid_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        arguments?.getParcelable<Post>(Constants.POST_ARGU_KEY)?.let { post ->
            arguments?.getString(Constants.API_TOKEN_ARGU_KEY)?.let { apiToken ->
                val viewModel: PostBidViewModel =
                    ViewModelProviders.of(this, PostBidViewModelFactory(projectRepo)).get(PostBidViewModel::class.java)
                post.id?.let {
                    viewModel.postBids(it, apiToken).subscribe {
                        post.bids = it
                        binding?.postBid?.adapter = MyAddsAdapter(
                            arrayListOf(post),
                            context,
                            object : MyAddsBidsAdapter.OnBidSelectedClickListener {
                                override fun onBidSelected(post: Post, bid: Bid) {
                                    compositeDisposable.add(viewModel.selectBid(bid, apiToken).subscribe {
                                        val fragment = ContractorDetailsFragment()
                                        post.bid = bid
                                        fragment.arguments = bundleOf(Pair(Constants.POST_ARGU_KEY, post))
                                        activity?.supportFragmentManager?.transaction {
                                            replace(R.id.fragment_container, fragment)
                                        }
                                    })
                                }
                            })
                        binding?.postBid?.layoutManager = LinearLayoutManager(context)
                    }
                }
            }
        }
        return binding?.root
    }

}