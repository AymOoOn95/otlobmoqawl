package com.yahya.otlobmokawl.data.repocontract

import com.yahya.otlobmokawl.data.entity.Membership
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.*
import io.reactivex.Observable

// user repo contract
interface IUserRepo {

    fun register(user: User, idToken: String): Observable<RegisterResponse>

    fun login(user: User): Observable<RegisterResponse>

    fun logout()

    fun saveUserData(user: User)

    fun getUserData(): Observable<User>

    fun updateUserData(user: User): Observable<User>

    fun changeLanguage(lang: String)

    fun getLanguage(): String

    fun getUserDataOnline(apiToken: String): Observable<UserDataResponse>

    fun myCredits(apiToken: String): Observable<UserCredit>

    fun saveCredit(userCredit: UserCredit)

    fun myCreditsFromShared(): Observable<UserCredit>

    fun purchasesReport(apiToken: String): Observable<PurchaseReportResponse>

    fun memberships(): Observable<ArrayList<Membership>>
    fun makePurchase(
        apiToken: String,
        sku: String,
        orderId: String,
        purchaseToken: String,
        signature: String,
        purchaseTime: String,
        price: String,
        currencyCode: String
    ): Observable<BuyCreditResponse>
}