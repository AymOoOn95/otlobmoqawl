package com.yahya.otlobmokawl.data.network.response

import com.yahya.otlobmokawl.data.entity.PurchaseItem

class PurchaseReportResponse(
    var status:Int,
    var invoices:ArrayList<PurchaseItem>
)