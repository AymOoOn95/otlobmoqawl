package com.yahya.otlobmokawl.ui.fragment.posts.favourites

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.network.response.MyListResponse
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class FavouritesViewModel(val userRepo: IUserRepo, val projectRepo: IProjectRepo) : ViewModel() {

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun favourites(apiToken: String): Observable<MyListResponse> {
        return projectRepo.getFavourites(apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun addPost2Fav(postId: Int, apiToken: String): Observable<RegisterResponse> {
        return projectRepo.addToFavourite(postId, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

}