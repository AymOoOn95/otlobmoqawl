package com.yahya.otlobmokawl.ui.activity.register

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.LanguageChange
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.Observer
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class RegisterActivity : AppCompatActivity() {
    val TAG = "RegisterActivity"
    @Inject
    lateinit var userRepo: IUserRepo

    val compositDisposable = CompositeDisposable()
    override fun onCreate(savedInstanceState: Bundle?) {
        LanguageChange.changeTo("ar", this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        (application as MyApplication).appComponent?.inject(this)
        val viewModel: RegisterViewModel =
            ViewModelProviders.of(this, RegisterViewModelFactory(userRepo)).get(RegisterViewModel::class.java)
        /*** declare variables ***/
        var areaId: Int? = -1
        var userType = Constants.USER_TYPE_CLIENT
        val user = User()
        /*** end declare variables ***/
        /*** find views ***/
        val clientType: RadioButton = findViewById(R.id.client_type)
        val contractorType: RadioButton = findViewById(R.id.contractor_type)
        val fullName: EditText = findViewById(R.id.full_name)
        val emailOrPHone: EditText = findViewById(R.id.email_or_phone)
        val password: EditText = findViewById(R.id.password)
        val countrySpinner: Spinner = findViewById(R.id.country_spinner)
        val citySpinner: Spinner = findViewById(R.id.city_spinner)
        val address: EditText = findViewById(R.id.address)
        val registerBtn: Button = findViewById(R.id.register)
        val loader: ProgressBar = findViewById(R.id.loader)
        /*** end find views ***/
        /*** initialize views ***/
        compositDisposable.add(
            viewModel.getLocations(null).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe(
                {
                    it.add(0, Address(-1, -1, "من فضلك اختر البلد"))
                    countrySpinner.adapter =
                        ArrayAdapter<Address>(this, R.layout.support_simple_spinner_dropdown_item, it)
                },
                {
                    Log.e(TAG, "An error occurred ", it)
                })
        )
        /*** end initialize views ***/

        /*** listeners ***/
        countrySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                parent?.let {
                    if (it.selectedItemPosition != 0) {
                        viewModel.getLocations((it.selectedItem as Address).id).subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread()).subscribe({
                                it.add(0, Address(-1, -1, "من فضلك اختر المنطقة"))
                                citySpinner.adapter = ArrayAdapter<Address>(
                                    this@RegisterActivity,
                                    R.layout.support_simple_spinner_dropdown_item,
                                    it
                                )
                            }, {
                                Log.e(TAG, "An error occurred", it)
                            })
                    }
                }
            }
        }

        citySpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                parent?.let {
                    if (it.selectedItemPosition != 0) {
                        areaId = (it.selectedItem as Address).id
                    }
                }
            }
        }

        clientType.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                userType = Constants.USER_TYPE_CLIENT
                contractorType.isChecked = false
            }
        }
        contractorType.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                userType = Constants.USER_TYPE_CONTRACTOR
                clientType.isChecked = false
            }
        }

        registerBtn.setOnClickListener { regBtn ->
            regBtn.visibility = View.GONE
            loader.visibility = View.VISIBLE
            if (fullName.text.toString().isNotEmpty() && emailOrPHone.text.toString().isNotEmpty() && password.text.toString().isNotEmpty() && areaId != -1) {
                user.name = fullName.text.toString()
                user.type = userType
                user.password = password.text.toString()
                user.email = emailOrPHone.text.toString()
                user.areaId = areaId
                user.address = address.text.toString()
                viewModel.userRegistration(user).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        if (it.status == 1) {
                            user.apiToken = it.apiToken
                            user.image = it.user?.image
                            viewModel.saveClientInShared(user)

                            it.apiToken?.let {
                                viewModel.userCreditOnline(it).subscribe(object : Observer<UserCredit> {
                                    override fun onComplete() {
                                        Log.d(TAG, "done getting user credits")
                                    }

                                    override fun onSubscribe(d: Disposable) {
                                        compositDisposable.add(d)
                                    }

                                    override fun onNext(t: UserCredit) {
                                        viewModel.saveCredits(t)
                                        regBtn.visibility = View.VISIBLE
                                        loader.visibility = View.GONE
                                        this@RegisterActivity.makeToast("تم التسجيل بنجاح")
                                        startActivity(Intent(this@RegisterActivity, MainActivity::class.java))


                                    }

                                    override fun onError(e: Throwable) {
                                        Log.e(TAG, "An error occurred ", e)
                                    }
                                })
                            }
                        }
                    }, {
                        this.makeToast("an error occurred")
                        Log.e(TAG, "An error occurred ", it)
                        regBtn.visibility = View.VISIBLE
                        loader.visibility = View.GONE
                    })
            } else {
                this.makeToast("يجب ان تملئ كل الحقول")
                regBtn.visibility = View.VISIBLE
                loader.visibility = View.GONE
            }

        }
        /*** end listeners ***/


    }

    override fun onDestroy() {
        super.onDestroy()
        compositDisposable.dispose()
    }

}
