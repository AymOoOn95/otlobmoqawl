package com.yahya.otlobmokawl.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
open class User(
    var name: String?,
    var email: String?,
    var password: String?,
    @SerializedName("phone_number")
    var phone: String?,
    var apiToken: String?,
    var rating: Int?,
    var image: String?,
    var type: String?,
    @SerializedName("area_id")
    var areaId: Int?,
    var address: String?,
    @SerializedName("nation_id")
    var sid: String?
) : Parcelable {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    )
}
