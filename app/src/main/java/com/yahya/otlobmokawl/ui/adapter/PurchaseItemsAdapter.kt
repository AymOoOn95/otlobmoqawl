package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.android.billingclient.api.SkuDetails
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.util.Constants

class PurchaseItemsAdapter(
    val skuDetails: List<SkuDetails>,
    val context: Context?,
    val onBuyItemClickListener: OnBuyItemClickListener
) :
    RecyclerView.Adapter<PurchaseItemsAdapter.PurchaseItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PurchaseItemViewHolder =
        PurchaseItemViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.in_app_purchase_item_layout,
                parent,
                false
            )
        )

    override fun getItemCount(): Int = skuDetails.size

    override fun onBindViewHolder(holder: PurchaseItemViewHolder, position: Int) {
        when (skuDetails[position].sku) {
            Constants.CLIENT_VIP_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.client_vip_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.client_vip_sku_desc)
            }
            Constants.CLIENT_BASIC_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.client_basic_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.client_basic_sku_desc)

            }

            Constants.CLIENT_MEDIUM_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.client_medium_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.client_medium_sku_desc)
            }

            Constants.CLIENT_EXTREM_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.client_extreme_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.client_extreme_sku_desc)
            }

            Constants.CONTRACTOR_VIP_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.contractor_vip_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.contractor_vip_sku_desc)
            }

            Constants.CONTRACTOR_BASIC_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.contractor_basic_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.contractor_basic_sku_desc)

            }

            Constants.CONTRACTOR_MEDIUM_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.contractor_medium_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.contractor_medium_sku_desc)

            }

            Constants.CONTRACTOR_EXTREM_SKU -> {
                holder.itemTitle.text = context?.resources?.getString(R.string.contractor_extreme_sku)
                holder.itemDesc.text = context?.resources?.getString(R.string.contractor_extreme_sku_desc)
            }
        }
        holder.itemPrice.text = skuDetails[position].price
        holder.buyItem.setOnClickListener {
            onBuyItemClickListener.onBuyClicked(skuDetails[position])
        }
    }

    interface OnBuyItemClickListener {
        fun onBuyClicked(item: SkuDetails)
    }

    class PurchaseItemViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemTitle: TextView = itemView.findViewById(R.id.item_title)
        val itemDesc: TextView = itemView.findViewById(R.id.item_desc)
        val itemPrice: TextView = itemView.findViewById(R.id.item_price)
        val buyItem: ImageView = itemView.findViewById(R.id.buy_item)
    }
}