package com.yahya.otlobmokawl.ui.fragment.contractor.update

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Contractor
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.network.response.UserDataResponse
import com.yahya.otlobmokawl.data.repo.ContractorRepo
import com.yahya.otlobmokawl.data.repo.UserRepo
import com.yahya.otlobmokawl.data.repocontract.IContractorRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody

class UpdateContractorViewModel(val userRepo: IUserRepo, val contractorRepo: IContractorRepo) : ViewModel() {

    fun userLocalData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun userOnlineData(apiToken: String): Observable<UserDataResponse> {
        return userRepo.getUserDataOnline(apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun locations(parentId: Int? = null): Observable<ArrayList<Address>> {
        return (userRepo as UserRepo).getLocations(parentId).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun updateContractor(contractor: Contractor, newPassword: String?,imagePart: MultipartBody.Part?): Observable<RegisterResponse> {
        return contractorRepo.updateContractor(contractor, newPassword,imagePart).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun saveContractorData(contractor: Contractor) {
        userRepo.saveUserData(contractor as User)
    }
}