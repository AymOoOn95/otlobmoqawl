package com.yahya.otlobmokawl.ui.activity.credit

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.BuyCreditResponse
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MyCreditViewModel(private val userRepo: IUserRepo) : ViewModel() {


    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun userCreditFromShared(): Observable<UserCredit> {
        return userRepo.myCreditsFromShared().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveUserCredit2Shared(userCredit: UserCredit) {
        userRepo.saveCredit(userCredit)
    }

    fun makePurchase(
        apiToken: String,
        sku: String,
        orderId: String,
        purchaseToken: String,
        signature: String,
        purchaseTime: String,
        price: String,
        currencyCode: String
    ): Observable<BuyCreditResponse> {
        return userRepo.makePurchase(
            apiToken,
            sku,
            orderId,
            purchaseToken,
            signature,
            purchaseTime,
            price,
            currencyCode
        ).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun getLang(): String {
        return userRepo.getLanguage()
    }
}