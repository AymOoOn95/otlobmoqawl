package com.yahya.otlobmokawl.ui.fragment.client.register

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.lifecycle.ViewModelProviders
import com.mlsdev.rximagepicker.RxImagePicker
import com.mlsdev.rximagepicker.Sources
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Address
import com.yahya.otlobmokawl.data.entity.Client
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.repocontract.IClientRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.UserRegisterFragmentBinding
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.util.*
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.MultipartBody
import javax.inject.Inject

class ClientRegisterFragment : Fragment() {
    val TAG = "ClientRegisterFragment"
    var binding: UserRegisterFragmentBinding? = null

    var compositeDisposable = CompositeDisposable()

    @Inject
    lateinit var userRepo: IUserRepo

    @Inject
    lateinit var clientRepo: IClientRepo

    var currentUser: User? = null

    var userType: String = Constants.USER_TYPE_CLIENT

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.user_register_fragment, container, false)
        var images: ArrayList<MultipartBody.Part?> = ArrayList()
        var userImage: MultipartBody.Part? = null
//        activity?.findViewById<ImageView>(R.id.user_image)?.setOnClickListener { imageView ->
//            RxImagePicker.with((context as AppCompatActivity).supportFragmentManager)
//                .requestImage(Sources.CHOOSER, "Choose Image").subscribe {
//                    (imageView as ImageView).setImageURI(it)
//                    userImage = it.toFile(context!!)?.toImagePart("image")
//                    images.add(userImage)
//
//                }
//        }
        binding?.let { nonNullableBinding ->
            (activity?.application as MyApplication).appComponent?.inject(this)
            val viewModel: ClientRegisterViewModel =
                ViewModelProviders.of(this, ClientRegisterViewModelFactory(userRepo, clientRepo))
                    .get(ClientRegisterViewModel::class.java)
            nonNullableBinding.viewModel = viewModel
            nonNullableBinding.userType.check(R.id.client_type)

            var idFace: MultipartBody.Part? = null
            var idBack: MultipartBody.Part? = null

            compositeDisposable.add(viewModel.userData().subscribe { user ->

                user.phone?.let {
                    nonNullableBinding.clientPhone.setText(user.phone)
                    nonNullableBinding.clientPhone.isEnabled = false
                }
                nonNullableBinding.clientName.setText(user.name)
                nonNullableBinding.clientPassword.setText("password")
                nonNullableBinding.clientPassword.visibility = View.GONE
                currentUser = user

            })
            nonNullableBinding.uploadFirstFace.setOnClickListener {
                RxImagePicker.with((context as AppCompatActivity).supportFragmentManager)
                    .requestImage(Sources.GALLERY, "Upload ID card face").subscribe {
                        idFace = it.toFile(context!!)?.toImagePart("nation_id_front")
                        images.add(idFace)
                        nonNullableBinding.firstFaceChecked.visibility = View.VISIBLE
                    }
            }

            nonNullableBinding.userType.setOnCheckedChangeListener { group, checkedId ->
                when (checkedId) {
                    R.id.contractor_type -> {
                        userType = Constants.USER_TYPE_CONTRACTOR
                    }
                    R.id.user_type -> {
                        userType = Constants.USER_TYPE_CLIENT
                    }
                }

                Log.d("Taaaag", "Type changes to $userType")
            }
            nonNullableBinding.uploadSecondFace.setOnClickListener {
                RxImagePicker.with((context as AppCompatActivity).supportFragmentManager)
                    .requestImage(Sources.GALLERY, "Upload ID card face").subscribe {
                        idBack = it.toFile(context!!)?.toImagePart("nation_id_back")
                        images.add(idBack)
                        nonNullableBinding.secondFaceChecked.visibility = View.VISIBLE
                    }
            }

            var areaId: Int? = null
            compositeDisposable.add(viewModel.getLocations(null).subscribeOn(Schedulers.io()).observeOn(
                AndroidSchedulers.mainThread()
            )
                .subscribe {
                    it.add(0, Address(-1, -1, "من فضلك اختر البلد"))
                    nonNullableBinding.countrySpinner.adapter =
                        ArrayAdapter<Address>(context, R.layout.support_simple_spinner_dropdown_item, it)
                    nonNullableBinding.countrySpinner.onItemSelectedListener =
                        object : AdapterView.OnItemSelectedListener {
                            override fun onNothingSelected(parent: AdapterView<*>?) {

                            }

                            override fun onItemSelected(
                                parent: AdapterView<*>?,
                                view: View?,
                                position: Int,
                                id: Long
                            ) {
                                if (position > 0) {
                                    compositeDisposable.add(viewModel.getLocations((parent?.selectedItem as Address).id)
                                        .subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                                        .subscribe {

                                            nonNullableBinding.citySpinner.adapter = ArrayAdapter<Address>(
                                                context,
                                                R.layout.support_simple_spinner_dropdown_item,
                                                it
                                            )

                                            nonNullableBinding.citySpinner.onItemSelectedListener =
                                                object : AdapterView.OnItemSelectedListener {
                                                    override fun onNothingSelected(parent: AdapterView<*>?) {

                                                    }

                                                    override fun onItemSelected(
                                                        parent: AdapterView<*>?,
                                                        view: View?,
                                                        position: Int,
                                                        id: Long
                                                    ) {
                                                        areaId = (parent?.selectedItem as Address).id
                                                    }
                                                }
                                        })
                                }
                            }
                        }
                })

            nonNullableBinding.registerClient.setOnClickListener { regBtn ->
                nonNullableBinding.loader.visibility = View.VISIBLE
                regBtn.visibility = View.GONE


                if (nonNullableBinding.clientName.text.isNotEmpty() &&
                    nonNullableBinding.clientEmail.text.isNotEmpty() &&
                    nonNullableBinding.clientPassword.text.isNotEmpty() &&
                    nonNullableBinding.clientPhone.text.isNotEmpty() &&
                    nonNullableBinding.clientAddress.text.isNotEmpty() &&
                    areaId != null
                ) {

                    val client = Client(
                        nonNullableBinding.clientName.text.toString(),
                        nonNullableBinding.clientEmail.text.toString(),
                        nonNullableBinding.clientPassword.text.toString(),
                        null,
                        null,
                        null,
                        null,
                        userType,
                        areaId,
                        nonNullableBinding.clientAddress.text.toString(),
                        null
                    )
                    if (currentUser != null) {
                        client.apiToken = currentUser?.apiToken
                        client.userType = userType
                    }
                    compositeDisposable.add(viewModel.register(
                        client,
                        images
                    ).subscribeOn(Schedulers.io()).observeOn(
                        AndroidSchedulers.mainThread()
                    ).subscribe {
                        if (it.status == 0) {
                            context?.makeToast(getString(R.string.used_email_phone))
                        } else {
                            client.apiToken = it.apiToken
                            client.type = userType
                            client.phone = it.user?.phone
                            client.image = it.user?.image
                            viewModel.saveClientInShared(client)
                            context?.makeToast(getString(R.string.register_done))
                            it.apiToken?.let {
                                viewModel.userCreditOnline(it).subscribe(object : Observer<UserCredit> {
                                    override fun onComplete() {
                                        Log.d(TAG, "done getting user credits")
                                    }

                                    override fun onSubscribe(d: Disposable) {
                                        compositeDisposable.add(d)
                                    }

                                    override fun onNext(t: UserCredit) {
                                        viewModel.saveCredits(t)
                                    }

                                    override fun onError(e: Throwable) {
                                        Log.e(TAG, "An error occurred ", e)
                                    }
                                })
                            }
                            (context as AppCompatActivity).supportFragmentManager.transaction {
                                startActivity(Intent(context, MainActivity::class.java))
                            }

                        }

                        nonNullableBinding.loader.visibility = View.GONE
                        regBtn.visibility = View.VISIBLE
                    })


                } else {
                    context?.makeToast(getString(R.string.fill_all_fields))
                    nonNullableBinding.loader.visibility = View.GONE
                    regBtn.visibility = View.VISIBLE

                }
            }
        }
        return binding?.root
    }


    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}