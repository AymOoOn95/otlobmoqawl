package com.yahya.otlobmokawl.ui.activity.initialregister

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.util.Constants
import com.yahya.otlobmokawl.util.LanguageChange
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import java.util.regex.Pattern
import javax.inject.Inject
import com.yahya.otlobmokawl.databinding.InitialRegisterLayoutBinding


class InitialRegisterActivity : AppCompatActivity() {

    @Inject
    lateinit var userRepo: IUserRepo

    var binding: InitialRegisterLayoutBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.initial_register_layout)
        (application as MyApplication).appComponent?.inject(this)
        val viewModel: InitialRegesterViewModel = ViewModelProviders.of(this, InitialRegisterViewModelFactory(userRepo))
            .get(InitialRegesterViewModel::class.java)
        LanguageChange.changeTo(viewModel.getLang(), this)
        binding?.viewModel = viewModel
        binding?.registerClient?.setOnClickListener {
            binding?.let { nbinding ->
                if (nbinding.clientPhone.text.toString().isNotEmpty() && nbinding.clientName.text.toString().isNotEmpty() && nbinding.clientPassword.text.toString().isNotEmpty()) {
                    val user = User()
                    user.phone = nbinding.clientPhone.text.toString()
                    user.name = nbinding.clientName.text.toString()
                    user.password = nbinding.clientPassword.text.toString()
                    user.type = Constants.USER_TYPE_ANONYMOUS
                    viewModel.registerAnonymousUser(user).subscribe { response ->
                        response.apiToken?.let {
                            user.apiToken = it
                            viewModel.saveUserData(user)
                            startActivity(Intent(this, MainActivity::class.java))
                            this.makeToast(getString(R.string.register_done))
                        } ?: this.makeToast(getString(R.string.register_faild))

                    }

                } else {
                    this.makeToast(getString(R.string.fill_all_fields))
                }
            }
        }
    }

    fun isEmailValid(email: String): Boolean {
        val expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$"
        val pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(email)
        return matcher.matches()
    }
}
