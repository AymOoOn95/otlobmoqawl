package com.yahya.otlobmokawl.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.transaction
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.ui.fragment.contractor.details.ContractorDetailsFragment
import com.yahya.otlobmokawl.ui.fragment.posts.details.PostDetailsFragment
import com.yahya.otlobmokawl.util.Constants

class AllPostsAdapter(
    val posts: ArrayList<Post>,
    val context: Context?,
    val categoryImage: String?,
    val favListener: OnFavButtonClickListener?,
    var hideFavIcon: Boolean = false
) :
    RecyclerView.Adapter<AllPostsAdapter.PostViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PostViewHolder {
        return PostViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.favourite_item_layout,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = posts.size

    fun deleteItem(position: Int) {
        posts.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(0, posts.size)
    }

    override fun onBindViewHolder(holder: PostViewHolder, position: Int) {
        context?.let {
            if (posts[position].isInFavourite == 1) {
                holder.favBtn.setImageDrawable(context.resources.getDrawable(R.drawable.ic_fav_filled))
            } else {
                holder.favBtn.setImageDrawable(context.resources.getDrawable(R.drawable.ic_empty_fav_primary))
            }
            Glide.with(it).load(posts[position].image).into(holder.projectImage)
            holder.projectTitle.text = posts[position].title
            holder.projectLocation.text = posts[position].address
            holder.favBtn.setOnClickListener {
                if (posts[position].isInFavourite == 1) {
                    posts[position].isInFavourite = 0
                } else {
                    posts[position].isInFavourite = 1
                }
                favListener?.onClick(posts[position], holder.favBtn, position)
            }
            holder.itemView.setOnClickListener {
                var fragment: Fragment? = null
                if (posts[position].isMine == 1 && posts[position].bid != null) {
                    fragment = ContractorDetailsFragment()
                } else {
                    fragment = PostDetailsFragment()
                }
                fragment.arguments = bundleOf(
                    Pair(Constants.POST_ARGU_KEY, posts[position]),
                    Pair(Constants.CATEGORY_ID_ARGUMENT_KEY, categoryImage)
                )
                (context as AppCompatActivity).supportFragmentManager.transaction {
                    replace(R.id.fragment_container, fragment)
                }
            }

            Glide.with(holder.categoryImage).load(posts[position].icon).into(holder.categoryImage)
            if (hideFavIcon) {
                holder.favBtn.visibility = View.GONE
            }
        }

        holder.projectSubcategory.text = posts[position].subCategoryStr
//        holder.projectPrice.text =

    }

    interface OnFavButtonClickListener {
        fun onClick(post: Post, faveBtn: ImageView, position: Int)
    }

    class PostViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val projectImage: ImageView
        val projectTitle: TextView
        val projectLocation: TextView
        val projectSubcategory: TextView
        //        val projectPrice: TextView
        val favBtn: ImageView
        val categoryImage: ImageView

        init {
            projectImage = itemView.findViewById(R.id.project_image)
            projectTitle = itemView.findViewById(R.id.project_title)
            projectLocation = itemView.findViewById(R.id.project_location)
            projectSubcategory = itemView.findViewById(R.id.project_subcategory)
//            projectPrice = itemView.findViewById(R.id.project_price)
            favBtn = itemView.findViewById(R.id.fav_btn)
            categoryImage = itemView.findViewById(R.id.category_image)
        }
    }
}