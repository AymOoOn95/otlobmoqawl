package com.yahya.otlobmokawl.ui.fragment.bid.view

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.Bid
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PostBidViewModel(val projectRepo: IProjectRepo) : ViewModel() {

    fun postBids(postId: Int, apiToken: String): Observable<ArrayList<Bid>> {
        return projectRepo.getProjectBids(postId, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }

    fun selectBid(bid: Bid, apiToken: String): Observable<Bid> {
        return projectRepo.selectBid(bid.id!!, apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}