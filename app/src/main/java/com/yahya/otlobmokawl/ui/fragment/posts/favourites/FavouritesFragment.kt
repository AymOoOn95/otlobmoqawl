package com.yahya.otlobmokawl.ui.fragment.posts.favourites

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.Post
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.FavouritesFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.AllPostsAdapter
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class FavouritesFragment : Fragment() {

    var binding: FavouritesFragmentBinding? = null
    @Inject
    lateinit var userRepo: IUserRepo

    @Inject
    lateinit var projectRepo: IProjectRepo

    val compositeDisposable = CompositeDisposable()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.favourites_fragment, container, false)
        (activity?.application as MyApplication).appComponent?.inject(this)
        val viewModel: FavouritesViewModel =
            ViewModelProviders.of(this, FavouritesViewModelFactory(userRepo, projectRepo))
                .get(FavouritesViewModel::class.java)

        binding?.viewModel = viewModel
        compositeDisposable.add(viewModel.userData().subscribe {
            it.apiToken?.let { apiToken ->
                compositeDisposable.add(viewModel.favourites(apiToken).subscribe {
                    binding?.loader?.visibility = View.GONE
                    if (it.status == 1) {
                        if (it.posts.isEmpty()) {
                            binding?.noItem?.visibility = View.VISIBLE
                        } else {
                            binding?.favouriteRecycler?.visibility = View.VISIBLE
                            binding?.favouriteRecycler?.adapter = AllPostsAdapter(it.posts, context, null,
                                object : AllPostsAdapter.OnFavButtonClickListener {
                                    override fun onClick(post: Post, faveBtn: ImageView, position: Int) {
                                        compositeDisposable.add(viewModel.addPost2Fav(post.id!!, apiToken).subscribe {
                                            if (it.status == 1 && it.isDeleted == 1) {
                                                (binding?.favouriteRecycler?.adapter as AllPostsAdapter).deleteItem(
                                                    position
                                                )
                                            }
                                        })
                                    }

                                })
                            binding?.favouriteRecycler?.layoutManager = LinearLayoutManager(context)
                            // todo fav adapter
                        }
                    } else {
                        context?.makeToast(getString(R.string.login_first))
                    }
                })
            }
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}