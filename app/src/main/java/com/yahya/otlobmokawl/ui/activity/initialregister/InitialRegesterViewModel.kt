package com.yahya.otlobmokawl.ui.activity.initialregister

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class InitialRegesterViewModel(val userRepo: IUserRepo) : ViewModel() {

    fun registerAnonymousUser(user: User): Observable<RegisterResponse> {
        return userRepo.register(user, "").subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun saveUserData(user: User) {
        userRepo.saveUserData(user)
    }

    fun getLang(): String {
        return userRepo.getLanguage()
    }

}