package com.yahya.otlobmokawl.ui.fragment.posts.myposts

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.network.response.MyListResponse
import com.yahya.otlobmokawl.data.repo.ProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MyPostsViewModel(val userRepo: IUserRepo, val projectRepo: IProjectRepo) : ViewModel() {

    fun userData(): Observable<User> {
        return userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }

    fun userProjects(apiToken: String): Observable<MyListResponse> {
        return (projectRepo as ProjectRepo).getUserProjects(apiToken).subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
    }
}