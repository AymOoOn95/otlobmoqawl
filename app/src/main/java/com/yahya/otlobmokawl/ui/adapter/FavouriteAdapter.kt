package com.yahya.otlobmokawl.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.yahya.otlobmokawl.R

class FavouriteAdapter : RecyclerView.Adapter<FavouriteAdapter.FavouriteViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavouriteViewHolder {
        return FavouriteViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.favourite_item_layout,parent,false))
    }

    override fun getItemCount(): Int = 7

    override fun onBindViewHolder(holder: FavouriteViewHolder, position: Int) {
    }

    class FavouriteViewHolder(val itemView:View) : RecyclerView.ViewHolder(itemView)
}