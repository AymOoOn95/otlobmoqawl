package com.yahya.otlobmokawl.ui.fragment.informative

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.yahya.otlobmokawl.data.repocontract.IUserRepo

class InformativeViewModelFactory(val userRepo: IUserRepo) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return InformativeViewModel(userRepo) as T
    }
}