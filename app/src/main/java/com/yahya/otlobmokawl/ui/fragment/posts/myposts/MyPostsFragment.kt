package com.yahya.otlobmokawl.ui.fragment.posts.myposts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.repocontract.IProjectRepo
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.databinding.MypostFragmentBinding
import com.yahya.otlobmokawl.ui.adapter.AllPostsAdapter
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class MyPostsFragment : Fragment() {

    @Inject
    lateinit var userRepo: IUserRepo

    @Inject
    lateinit var projectRepo: IProjectRepo

    val compositeDisposable = CompositeDisposable()
    var binding: MypostFragmentBinding? = null
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        (activity?.application as MyApplication).appComponent?.inject(this)
        binding = DataBindingUtil.inflate(inflater, R.layout.mypost_fragment, container, false)
        val viewModel: MyPostsViewModel = ViewModelProviders.of(this, MyPostsViewModelFactory(userRepo, projectRepo))
            .get(MyPostsViewModel::class.java)
        binding?.viewModel = viewModel
        compositeDisposable.add(viewModel.userData().subscribe {
            it.apiToken?.let {
                compositeDisposable.add(viewModel.userProjects(it).subscribe {
                    binding?.loader?.visibility = View.GONE
                    if (it.status == 1) {
                        if (it.posts.isEmpty()) {
                            binding?.noItem?.visibility = View.VISIBLE
                        } else {
                            binding?.myposts?.adapter = AllPostsAdapter(it.posts, context, null, null, true)
                            binding?.myposts?.layoutManager = LinearLayoutManager(context)
                            binding?.myposts?.visibility = View.VISIBLE
                        }
                    } else {
                        context?.makeToast(getString(R.string.login_first))
                    }
                })
            }
        })
        return binding?.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        compositeDisposable.dispose()
    }
}