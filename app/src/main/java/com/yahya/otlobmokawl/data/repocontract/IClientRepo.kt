package com.yahya.otlobmokawl.data.repocontract

import com.yahya.otlobmokawl.data.entity.*
import com.yahya.otlobmokawl.data.network.response.RegisterResponse
import io.reactivex.Observable
import okhttp3.MultipartBody

interface IClientRepo {
    fun answerQuestion(question: Question, apiToken: String): Observable<Question>
    fun acceptBid(bid: Bid)
    fun finishProject(rate: Int)
    fun updateClientData(
        client: Client,
        newPassword: String?,
        imagePart: MultipartBody.Part?
    ): Observable<RegisterResponse>

    fun registerClient(client: Client, images: ArrayList<MultipartBody.Part?>): Observable<RegisterResponse>
    fun questions4Me(apiToken: String): Observable<ArrayList<Question>>
}