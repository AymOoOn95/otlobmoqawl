package com.yahya.otlobmokawl.ui.activity.login

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable

class LoginViewModel(val userRepo:IUserRepo) : ViewModel(){

    fun writeUser2Shared(user: User){
        userRepo.saveUserData(user)
    }

    fun getUserFromShared():Observable<User>{
        return userRepo.getUserData()
    }
}