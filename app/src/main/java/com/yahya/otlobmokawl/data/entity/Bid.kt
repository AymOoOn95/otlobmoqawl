package com.yahya.otlobmokawl.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Bid(
    var id: Int?,
    @SerializedName("post_id")
    var postId: Int?,
    @SerializedName("user_id")
    var userId: Int?,
    @SerializedName("bid")
    var bid: String?,
    @SerializedName("created_at")
    var date: String?,
    var contractor: Contractor?,
    var status: Int?
) : Parcelable {
}