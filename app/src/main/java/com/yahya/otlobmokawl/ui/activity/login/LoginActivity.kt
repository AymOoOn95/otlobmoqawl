package com.yahya.otlobmokawl.ui.activity.login

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.firebase.ui.auth.IdpResponse
import android.app.Activity
import android.util.Log
import androidx.lifecycle.ViewModelProviders
import com.yahya.otlobmokawl.R
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import com.yahya.otlobmokawl.ui.activity.main.MainActivity
import com.yahya.otlobmokawl.util.LanguageChange
import com.yahya.otlobmokawl.util.MyApplication
import com.yahya.otlobmokawl.util.makeToast
import javax.inject.Inject


class LoginActivity : AppCompatActivity() {

    val TAG = "LoginActivity"
    val RC_SIGN_IN = 1166

    @Inject
    lateinit var repo: IUserRepo
    lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LanguageChange.changeTo("ar", this)
        setContentView(R.layout.activity_login)
        (application as MyApplication).appComponent?.inject(this)
        viewModel = ViewModelProviders.of(this, LoginViewModelFactory(repo)).get(LoginViewModel::class.java)
        viewModel.getUserFromShared().subscribe({
            it.name?.let {
                this.makeToast("welcome back, $it")
                startActivity(Intent(this, MainActivity::class.java))
            } ?: run {
                val providers = arrayListOf(
                    AuthUI.IdpConfig.EmailBuilder().build(),
                    AuthUI.IdpConfig.GoogleBuilder().build(),
                    AuthUI.IdpConfig.FacebookBuilder().build()
                )

                startActivityForResult(
                    AuthUI.getInstance()
                        .createSignInIntentBuilder()
                        .setAvailableProviders(providers)
                        .setLogo(R.drawable.ic_logo)
                        .build(),
                    RC_SIGN_IN
                )
            }
        }, {
            Log.e(TAG, it.message)
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode === RC_SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)

            if (resultCode === Activity.RESULT_OK) {
                // todo write user to shared
                FirebaseAuth.getInstance().currentUser?.let {
                    val user = User(it.displayName, it.email, null, null, null, null, null, null, null, null, null)
                    viewModel.writeUser2Shared(user)
                }

            } else {
                this.makeToast("Failed to log in")
            }
        }

    }
}
