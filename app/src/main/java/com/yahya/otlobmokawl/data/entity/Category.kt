package com.yahya.otlobmokawl.data.entity

import com.google.gson.annotations.SerializedName

class Category(
    var id: Int?,
    @SerializedName("parent_id")
    var parentId: Int?,
    @SerializedName("is_featured")
    var isFeatured: Int?,
    var position: Int?,
    var image: String?,
    var name: String?
) {
    override fun toString(): String {
        return this.name ?: ""
    }
}
