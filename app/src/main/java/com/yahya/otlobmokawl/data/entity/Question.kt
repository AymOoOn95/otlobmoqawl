package com.yahya.otlobmokawl.data.entity

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class Question(
    var id: Int?,
    var status: Int?,
    var title: String?,
    var question: String?,
    var answer: String?,
    @SerializedName("user_id")
    var userId: Int?,
    @SerializedName("reciever_id")
    var recieverId: Int?,
    @SerializedName("post_id")
    var postId: Int?,
    @SerializedName("created_at")
    var createdAt: String?,
    @SerializedName("updated_at")
    var updatedAt: String?


) : Parcelable