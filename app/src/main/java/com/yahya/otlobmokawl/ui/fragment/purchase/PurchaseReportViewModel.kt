package com.yahya.otlobmokawl.ui.fragment.purchase

import androidx.lifecycle.ViewModel
import com.yahya.otlobmokawl.data.entity.User
import com.yahya.otlobmokawl.data.entity.UserCredit
import com.yahya.otlobmokawl.data.network.response.PurchaseReportResponse
import com.yahya.otlobmokawl.data.repocontract.IUserRepo
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class PurchaseReportViewModel(private val userRepo: IUserRepo) : ViewModel() {

    fun userData(): Observable<User> =
        userRepo.getUserData().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())

    fun userCredit(): Observable<UserCredit> {
        return userRepo.myCreditsFromShared().subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
    }


    fun userPurchasesReport(apiToken: String): Observable<PurchaseReportResponse> =
        userRepo.purchasesReport(apiToken).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}